namespace MDRIP.Migrations
{
	using System.Data.Entity.Migrations;

	public partial class AlertUpdate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Alerts", "User_ID", c => c.String());
            DropColumn("dbo.Alerts", "User");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Alerts", "User", c => c.String());
            DropColumn("dbo.Alerts", "User_ID");
        }
    }
}
