namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class featured : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Occurrences",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        ReportedLocation = c.String(),
                        District = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Prevalence = c.Int(),
                        DeathRate = c.Int(),
                        Bacteria_ID = c.String(),
                        Infection_ID = c.String(),
                        User_ID = c.String(),
                    })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.OccurrenceAttachments",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    Occurrence_ID = c.String(),
                    Name = c.String(),
                    Link = c.String(),
                    User_ID = c.String(),
                })
                .PrimaryKey(t => t.ID);


        }
        
        public override void Down()
        {
            
            DropTable("dbo.FeaturedItems");
            DropTable("dbo.OccurrenceAttachments");
            
        }
    }
}
