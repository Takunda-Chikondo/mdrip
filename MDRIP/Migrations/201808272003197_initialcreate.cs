namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialcreate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "HouseAndStreet");
            DropColumn("dbo.AspNetUsers", "Region");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Region", c => c.String());
            AddColumn("dbo.AspNetUsers", "HouseAndStreet", c => c.String());
        }
    }
}
