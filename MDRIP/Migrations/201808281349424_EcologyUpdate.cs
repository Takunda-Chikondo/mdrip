namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EcologyUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BacterialEcologies", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.BacterialEcologies", new[] { "User_Id" });
            AlterColumn("dbo.BacterialEcologies", "User_ID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BacterialEcologies", "User_ID", c => c.String(maxLength: 128));
            CreateIndex("dbo.BacterialEcologies", "User_Id");
            AddForeignKey("dbo.BacterialEcologies", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
