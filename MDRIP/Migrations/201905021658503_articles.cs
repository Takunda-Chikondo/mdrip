namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class articles : DbMigration
    {
        public override void Up()
        {
                       
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        PublishDate = c.DateTime(nullable: false),
                        SequenceNumber = c.Int(nullable: false),
                        User_ID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
           
            
        }
        
        public override void Down()
        {            
            DropTable("dbo.Articles");
        }
    }
}
