namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AntibiotictUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Antibiotics", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Antibiotics", new[] { "User_Id" });
            AlterColumn("dbo.Antibiotics", "User_ID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Antibiotics", "User_ID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Antibiotics", "User_Id");
            AddForeignKey("dbo.Antibiotics", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
