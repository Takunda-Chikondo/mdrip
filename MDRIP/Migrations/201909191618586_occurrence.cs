namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class occurrence : DbMigration
    {
        public override void Up()
        {
            
            CreateTable(
                "dbo.Occurrences",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        ReportedLocation = c.String(),
                        District = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Prevalence = c.Long(nullable: false),
                        DeathRate = c.Long(nullable: false),
                        User_ID = c.String(),
                        Bacteria_ID = c.Int(),
                        Infection_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Bacteria", t => t.Bacteria_ID)
                .ForeignKey("dbo.Infections", t => t.Infection_ID)
                .Index(t => t.Bacteria_ID)
                .Index(t => t.Infection_ID);
            
            
            CreateTable(
                "dbo.OccurrenceAttachments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Link = c.String(nullable: false),
                        Occurrence_ID = c.Int(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Occurrences", t => t.Occurrence_ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.Occurrence_ID)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OccurrenceAttachments", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.OccurrenceAttachments", "Occurrence_ID", "dbo.Occurrences");
            DropForeignKey("dbo.Occurrences", "Infection_ID", "dbo.Infections");
            DropForeignKey("dbo.Occurrences", "Bacteria_ID", "dbo.Bacteria");
            DropIndex("dbo.OccurrenceAttachments", new[] { "User_Id" });
            DropIndex("dbo.OccurrenceAttachments", new[] { "Occurrence_ID" });
            DropIndex("dbo.Occurrences", new[] { "Infection_ID" });
            DropIndex("dbo.Occurrences", new[] { "Bacteria_ID" });
            DropTable("dbo.OccurrenceAttachments");
            DropTable("dbo.Occurrences");
        }
    }
}
