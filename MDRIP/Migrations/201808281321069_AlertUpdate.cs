namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlertUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Alerts", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Alerts", new[] { "User_Id" });
            AddColumn("dbo.Alerts", "User", c => c.String());
            DropColumn("dbo.Alerts", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Alerts", "User_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.Alerts", "User");
            CreateIndex("dbo.Alerts", "User_Id");
            AddForeignKey("dbo.Alerts", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
