namespace MDRIP.Migrations
{
    using MDRIP.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MDRIP.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "MDRIP.Models.ApplicationDbContext";
        }

        protected override void Seed(MDRIP.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Roles.AddOrUpdate(new IdentityRole() { Name = "General User", Id="1" });
            context.Roles.AddOrUpdate(new IdentityRole() { Name = "Admin", Id="2" });
            context.Roles.AddOrUpdate(new IdentityRole() { Name = "ResearcherScientist", Id = "3" });

            context.UserTypes.AddOrUpdate(new UserType() { ID=1, Name="Researcher"});
            context.UserTypes.AddOrUpdate(new UserType() { ID = 1, Name = "Scientist" });

            context.Centers.AddOrUpdate(new Center() {
                ID=1,
                Name="Quaid-I-Azam University",
                Email="noyetset",
                Type="University"
            });

            context.Centers.AddOrUpdate(new Center()
            {
                ID = 2,
                Name = "University of Cape Town",
                Email = "noyetset",
                Type = "University"
            });

            context.SaveChanges();
            PasswordHasher passwordHasher = new PasswordHasher();

            context.Users.AddOrUpdate(new ApplicationUser()
            {
                Id = "1",
                UserName = "master@mdrip.mdrip",
                Email = "master@mdrip.mdrip",
                FirstName = "Master Account",
                LastName = "MDRIP",
                Activated = true,
                Country = "Pakistan",
                Center = context.Centers.Find(1),
                Type = context.UserTypes.Find(1),
                EmailConfirmed = true,
                SecurityStamp="None",
                PasswordHash = passwordHasher.HashPassword("123456aA!")
            });

            var store = new UserStore<ApplicationUser>(context);
            var manager = new UserManager<ApplicationUser>(store);

			context.SaveChanges();
			manager.AddToRole("1", "Admin");

        }
    }
}
