namespace MDRIP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountryGeojson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Countries", "Geojson", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Countries", "Geojson");
        }
    }
}
