﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MDRIP
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
           // config.MapHttpAttributeRoutes();

           // config.Routes.MapHttpRoute(
           //     name: "APISearchInfection",
           //     routeTemplate: "api/searchinfection/{id}",
           //     defaults: new { controller = "API", id = RouteParameter.Optional }
           // );
           // config.Routes.MapHttpRoute(
           //    name: "APISearchRegion",
           //    routeTemplate: "api/searchregion/{id}",
           //    defaults: new { controller = "API", id = RouteParameter.Optional }
           //);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
