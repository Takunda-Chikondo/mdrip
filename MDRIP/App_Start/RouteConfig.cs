﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MDRIP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Index", action = "Home", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "map",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Visualisation", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Occurences",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Occurences", action = "Get", id = UrlParameter.Optional }
           );
        }
    }
}
