﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="search_box.ascx.cs" Inherits="MDRIP.Views.Controls.search_box" %>
<div class="input-group">
    <asp:TextBox ID="txtSearch"  class="form-control" runat="server"></asp:TextBox>
    <div class="input-group-append">
        <span class="input-group-text"><i class="now-ui-icons search"></i></span>
    </div>
</div>