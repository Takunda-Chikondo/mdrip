﻿using System.Collections.Generic;
using System.Globalization;
using System.Security.Claims;

namespace MDRIP.helper
{
    public static class Helper
    {
        /// <summary>
        /// Returns the first name of the user
        /// </summary>
        /// <param name="usr"></param>
        /// <returns></returns>
        public static string GetFirstName(this System.Security.Principal.IPrincipal usr)
        {
            var firstNameClaim = ((ClaimsIdentity)usr.Identity).FindFirst("FirstName");
            if (firstNameClaim != null)
            {
                return firstNameClaim.Value;
            }
            return usr.Identity.Name;
        }

        /// <summary>
        /// Returns a list of countries that exist - uses the ASP culture
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCountries() {
            List<string> CultureList = new List<string>();
            CultureInfo[] cultureInfos = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach (CultureInfo culture in cultureInfos)
            {
                RegionInfo regionInfo = new RegionInfo(culture.LCID);
                if (!(CultureList.Contains(regionInfo.EnglishName)))
                {
                    CultureList.Add(regionInfo.EnglishName);
                }
            }
            CultureList.Sort();
            return CultureList;
        }
    }

}