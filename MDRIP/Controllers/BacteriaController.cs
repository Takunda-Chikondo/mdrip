﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MDRIP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MDRIP.Controllers
{
    [Authorize]
    public class BacteriaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

		#region UserManager
		private ApplicationUserManager _userManager;

		public BacteriaController()
		{
		}
		public BacteriaController(ApplicationUserManager userManager)
		{
			UserManager = userManager;
		}

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}
		#endregion

		// GET: Bacteria
		public ActionResult Index(string searchString)
        {

            var user = UserManager.FindById(User.Identity.GetUserId());

            var bacteria = from c in db.Bacteria.Include(item => item.Attachments).Include(i => i.Infections).Include(i=>i.Infections) select c;
			if (!String.IsNullOrEmpty(searchString))
			{
				bacteria = bacteria.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
			}

            if (User.IsInRole("ResearcherScientist"))
            {
                return View(bacteria.Where(i => i.User_ID == user.Id).ToList());
            }
            else
            {
                return View(bacteria);
            }
		}

        // GET: Bacteria/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bacteria bacteria = db.Bacteria.Include(b => b.Attachments).Where(b => b.ID == id).FirstOrDefault();
            if (bacteria == null)
            {
                return HttpNotFound();
            }
            return View(bacteria);
        }

        // GET: Bacteria/Create
        public ActionResult Create()
        {
			List<SelectListItem> list = new List<SelectListItem>();

			foreach (Infection e in db.Infections)
			{
				SelectListItem item = new SelectListItem()
				{
					Text = e.Name,
					Value = e.ID.ToString(),
					Selected = false
				};
				list.Add(item);
			};
				BacteriaViewModel model = new BacteriaViewModel
			{
				InfectionsList = list,
			};
            return View(model);
        }

        // POST: Bacteria/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BacteriaViewModel model)
        {
           
            if (ModelState.IsValid)
            {
				model.Bacteria.Infections = new List<Infection>();
				var user = UserManager.FindById(User.Identity.GetUserId());

				if (model.SelectedInfectionsList != null)
				{
					foreach (var id in model.SelectedInfectionsList)
					{
						var item = db.Infections.Find(Int32.Parse(id));
						if (item != null)
						{
							model.Bacteria.Infections.Add(item);
						}
					};
				}
		
				model.Bacteria.User_ID = User.Identity.GetUserId();
				db.Bacteria.Add(model.Bacteria);
                db.SaveChanges();

                SaveAttachments(model.Bacteria);
                
                return RedirectToAction("Index");
            }

			List<SelectListItem> list = new List<SelectListItem>();

			foreach (Infection e in db.Infections)
			{
				SelectListItem item = new SelectListItem()
				{
					Text = e.Name,
					Value = e.ID.ToString(),
					Selected = false
				};
				list.Add(item);
			};
			BacteriaViewModel bvm = new BacteriaViewModel
			{
				InfectionsList = list,
			};
			return View(bvm);
        }

        // GET: Bacteria/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			List<SelectListItem> list = new List<SelectListItem>();

			var bacteria = db.Bacteria.Where(item => item.ID == id).Include(item => item.Attachments).Include(i => i.Infections).First();

			foreach (Infection e in db.Infections)
			{
				
				SelectListItem item = new SelectListItem()
				{
					Text = e.Name,
					Value = e.ID.ToString(),
					Selected = (bacteria.Infections.Contains(e)?true:false)
				};
				list.Add(item);
			};

			BacteriaViewModel model = new BacteriaViewModel
			{
				Bacteria = bacteria,
				InfectionsList = list,
			};

			if (model.Bacteria == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

		// POST: Bacteria/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(BacteriaViewModel model)
		{

			if (ModelState.IsValid)
			{
				model.Bacteria.Infections = new List<Infection>();
				//var user = UserManager.FindById(User.Identity.GetUserId());

				foreach (var id in model.SelectedInfectionsList)
				{
					var item = db.Infections.Find(Int32.Parse(id));
					if (item != null)
					{
						model.Bacteria.Infections.Add(item);
					}
				};

				model.Bacteria.User_ID = User.Identity.GetUserId();
				db.Bacteria.Add(model.Bacteria);
				db.SaveChanges();

				SaveAttachments(model.Bacteria);
				///db.Entry(bacteria).State = EntityState.Modified;
				db.SaveChanges();
				SaveAttachments(model.Bacteria);

				return RedirectToAction("Index");
			}
			return View(model);
		}

			// GET: Bacteria/Delete/5
			public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bacteria bacteria = db.Bacteria.Include(b=>b.Attachments).Where(b=>b.ID == id).FirstOrDefault();
            if (bacteria == null)
            {
                return HttpNotFound();
            }
            return View(bacteria);
        }

        // POST: Bacteria/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var bacteria = db.Bacteria.Include(item=>item.Attachments).Include(b=>b.Occurrences).Where(i=>i.ID==id).First();

            foreach (var att in bacteria.Attachments)
            {
                var path = Request.MapPath("~/" + att.Link);
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }

            db.Bacteria.Remove(bacteria);
            
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

		private void SaveAttachments(Bacteria bacteria)
		{
			var subPath = "Attachments/Bacteria/" + bacteria.ID + "/";
			bool exists = Directory.Exists(Server.MapPath("~/" + subPath));
			if (!exists)
			{
				Directory.CreateDirectory(Server.MapPath("~/" + subPath));
			}
			for (var i = 0; i < Request.Files.Count; i++)
			{
				HttpPostedFileBase hpf = Request.Files[i];
				var fileName = Path.GetFileName(hpf.FileName);
				if (fileName == null || fileName == "")
				{
					break;
				}

				var path = Path.Combine(subPath, fileName);
				hpf.SaveAs(Server.MapPath("~/" + path));
				BacteriaAttachment a = new BacteriaAttachment()
				{
					Name = fileName,
					Bacteria = bacteria,
					Link = path
				};
				db.BacteriaAttachments.Add(a);
				db.SaveChanges();
			}
		}

		public ActionResult NameExists(string Name, int? Id)
		{
			var validateName = db.Bacteria.FirstOrDefault(x => x.Name == Name && x.ID != Id);

			if (validateName != null)
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}
		}
	}
}
