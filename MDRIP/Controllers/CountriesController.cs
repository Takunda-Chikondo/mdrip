﻿using System;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MDRIP.helper;
using MDRIP.Models;
using Newtonsoft.Json.Linq;

namespace MDRIP.Controllers
{
	[Authorize(Roles = "Admin")]
	public class CountriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Countries
        public ActionResult Index()
        {
            var list = db.Countries.ToList();
            list.Sort((x, y) =>
                    x.Name.CompareTo(y.Name));
            return View(list);
        }

        // GET: Countries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			Country country = db.Countries.Where(c => c.ID == id).Include(c => c.Regions).FirstOrDefault();
			country.Regions = country.Regions.OrderBy(r => r.Name).ToList();
			if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // GET: Countries/Create
        public ActionResult Create()
        {
            return View(new CountryViewModel() { Countries = Helper.GetCountries() });
        }

        // POST: Countries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Country country)
        {
            if (ModelState.IsValid)
            {
                var subPath = "Assets/map";
                bool exists = System.IO.Directory.Exists(Server.MapPath("~/" + subPath));
                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(Server.MapPath("~/" + subPath));
                }
                HttpPostedFileBase hpf = Request.Files[0];
                var fileName = country.Name.ToLower().Trim() + ".json";
                var path = System.IO.Path.Combine(subPath, fileName);
                var fullPath = Server.MapPath("~/" + path);
                hpf.SaveAs(fullPath);
                country.Geojson = fileName;
                db.Countries.Add(country);
                db.SaveChanges();
                ReadJson(fullPath, country);
                return RedirectToAction("Index");
            }

            return View(country);
        }

        public void ReadJson(string path, Country country) {

            var json = System.IO.File.ReadAllText(path);
            try
            {
                var jObject = JObject.Parse(json);

                if (jObject != null)
                {
                    JArray features = (JArray)jObject["features"];
                    for (var i = 0; i < features.Count; i++) {
                        var feature = features[i];
                        var properties = feature["properties"];
                        var district = properties["districts"].ToString();
                        db.Regions.Add(new Models.Region()
                        {
                            Name = district,
                            Country = country
                        });
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
            }
        }

        // GET: Countries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = db.Countries.Where(c=> c.ID == id).Include(c => c.Regions).FirstOrDefault();
            country.Regions = country.Regions.OrderBy(r=>r.Name).ToList();
            if (country == null)
            {
                return HttpNotFound();
            }
            
            return View(new CountryViewModel() { Countries = Helper.GetCountries(), Country = country });
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Country country)
        {
            if (ModelState.IsValid)
            {
                var fileName = country.Name.ToLower().Trim() + ".json";
                country.Geojson = fileName;
                db.Entry(country).State = EntityState.Modified;
                db.SaveChanges();
                try
                {
                    if (Request.Files.Count > 0)
                    {
                        db.Regions.RemoveRange(db.Regions.Include(r => r.Country).Where(r => r.Country.ID == country.ID));
                        db.SaveChanges();

                        var subPath = "Assets/map";
                        bool exists = System.IO.Directory.Exists(Server.MapPath("~/" + subPath));
                        if (!exists)
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/" + subPath));
                        }
                        HttpPostedFileBase hpf = Request.Files[0];
                        var path = System.IO.Path.Combine(subPath, fileName);
                        var fullPath = Server.MapPath("~/" + path);
                        hpf.SaveAs(fullPath);
                        ReadJson(fullPath, country);
                    }
                }
                catch (Exception e) {
                    throw e;
                    //
                }

                return RedirectToAction("Index");
            }
            return View(country);
        }

        // GET: Countries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = db.Countries.Where(c => c.ID == id).Include(c => c.Regions).FirstOrDefault();
            country.Regions = country.Regions.OrderBy(r => r.Name).ToList(); if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Country country = db.Countries.Include(r=>r.Regions).Where(c=>c.ID == id).First();
            var path = Request.MapPath("~/Assets/map/" +country.Geojson);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            db.Countries.Remove(country);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
