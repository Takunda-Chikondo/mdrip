﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MDRIP.Models;

namespace MDRIP.Controllers
{
    [Authorize]
    public class OccurrenceAttachmentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: BacteriaAttachments
        public ActionResult Index()
        {
            return View(db.OccurrenceAttachments.ToList());
        }

        // GET: BacteriaAttachments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OccurrenceAttachment occurrenceAttachment = db.OccurrenceAttachments.Find(id);
            if (occurrenceAttachment == null)
            {
                return HttpNotFound();
            }
            return View(occurrenceAttachment);
        }

        // GET: BacteriaAttachments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BacteriaAttachments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Link")] OccurrenceAttachment occurrenceAttachment)
        {
            if (ModelState.IsValid)
            {
                db.OccurrenceAttachments.Add(occurrenceAttachment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(occurrenceAttachment);
        }

        // GET: BacteriaAttachments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OccurrenceAttachment occurrenceAttachment = db.OccurrenceAttachments.Find(id);
            if (occurrenceAttachment == null)
            {
                return HttpNotFound();
            }
            return View(occurrenceAttachment);
        }

        // POST: BacteriaAttachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Link")] OccurrenceAttachment occurrenceAttachment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(occurrenceAttachment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(occurrenceAttachment);
        }

        // GET: BacteriaAttachments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OccurrenceAttachment occurrenceAttachment = db.OccurrenceAttachments.Find(id);
            if (occurrenceAttachment == null)
            {
                return HttpNotFound();
            }
            return View(occurrenceAttachment);
        }

        // POST: BacteriaAttachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OccurrenceAttachment occurrenceAttachment = db.OccurrenceAttachments.Find(id);
            db.OccurrenceAttachments.Remove(occurrenceAttachment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteAttachment(int id)
        {
            OccurrenceAttachment occurrenceAttachment = db.OccurrenceAttachments.Include(x=>x.Occurrence).Where(x=>x.ID == id).SingleOrDefault();
            if (occurrenceAttachment == null) {
                return null;
            }
            var occurrenceID = occurrenceAttachment.Occurrence.ID;

            var path = Request.MapPath("~/" + occurrenceAttachment.Link);
            if (System.IO.File.Exists(path)) {
                System.IO.File.Delete(path);
            }
            db.OccurrenceAttachments.Remove(occurrenceAttachment);
            db.SaveChanges();
            Response.Redirect(Url.Action("Edit", "Occurrences") + "/" + occurrenceID.ToString());

            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
