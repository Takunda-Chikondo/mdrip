﻿using MDRIP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MDRIP.Controllers
{
    [Authorize(Roles = "Admin, ResearcherScientist")]
    public class BacterialEcologiesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        #region UserManager
        private ApplicationUserManager _userManager;

        public BacterialEcologiesController()
        {
        }
        public BacterialEcologiesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        // GET: BacterialEcologies
        public ActionResult Index(string searchString)
        {
			{
                var user = UserManager.FindById(User.Identity.GetUserId());

                var ecology = from c in db.BacterialEcologies select c;
				if (!String.IsNullOrEmpty(searchString))
				{
					ecology = ecology.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
				}
                if (User.IsInRole("ResearcherScientist"))
                {
                    return View(ecology.Where(i => i.User_ID == user.Id).ToList());
                }
                else
                {
                    return View(ecology);
                }
            }
        }

        // GET: BacterialEcologies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BacterialEcology bacterialEcology = db.BacterialEcologies.Find(id);
            if (bacterialEcology == null)
            {
                return HttpNotFound();
            }
            return View(bacterialEcology);
        }

        // GET: BacterialEcologies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BacterialEcologies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,District,Habitat,Prevention")] BacterialEcology bacterialEcology)
        {
            if (ModelState.IsValid)
            {
				bacterialEcology.User_ID = User.Identity.GetUserId();
				db.BacterialEcologies.Add(bacterialEcology);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bacterialEcology);
        }

        // GET: BacterialEcologies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BacterialEcology bacterialEcology = db.BacterialEcologies.Find(id);
            if (bacterialEcology == null)
            {
                return HttpNotFound();
            }
            return View(bacterialEcology);
        }

        // POST: BacterialEcologies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,District,Habitat,Prevention")] BacterialEcology bacterialEcology)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bacterialEcology).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bacterialEcology);
        }

        // GET: BacterialEcologies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BacterialEcology bacterialEcology = db.BacterialEcologies.Find(id);
            if (bacterialEcology == null)
            {
                return HttpNotFound();
            }
            return View(bacterialEcology);
        }

        // POST: BacterialEcologies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BacterialEcology bacterialEcology = db.BacterialEcologies.Find(id);
            db.BacterialEcologies.Remove(bacterialEcology);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
