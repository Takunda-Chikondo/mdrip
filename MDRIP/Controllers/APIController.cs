﻿using MDRIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web.Http;
using System.IO;
using System.Web;

namespace MDRIP.Controllers
{
    [Authorize]
    public class APIController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        
        /// <summary>
        /// This method takes in a timeperiod, some infections, some bacteria and some regions.
        /// It then makes database call to fetch visualisation data related to the input
        /// and then converts, that in the way that is required on the front end.
        /// 
        /// First the bacteria table is searched, then the infection table and all that are needed are added to a list.
        /// For each element in the list, the occurrences of the element are searched and aggregated together.
        /// While doing the list iterations, some totals are done that are used on the front end.
        /// The list of infections/bacteria together with their aggreagted occurrences, the list of geojson files names
        /// are returned to the front end where they are deserialised and used to render the visualisations.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public object GetVisualisationsData(VisualisationData data)
        {

            DateTime startDate = DateTime.Parse(data.StartDate);
            DateTime endDate = DateTime.Parse(data.EndDate);
            List<Object> returnDatas = new List<Object>();
            List<string> countriesGeo = new List<string>();
            long prevGrandTotal = 0;
            long dtGrandTotal = 0;
            foreach (string reg in data.Regions)
            {
                var regionData = new RegionDataBundle();

                var region = db.Regions.Include(r => r.Country).Where(r => r.ID.ToString() == reg).First();

                regionData.RegionName = region.Name;
                regionData.RegionID = region.ID;

                countriesGeo.Add(region.Country.Geojson);

                List<InfBactData> infBactDatas = new List<InfBactData>();

                var infData = new InfBactData();
                int? previous = -1;
                List<Occur> occurs = new List<Occur>();
                List<Occur> copyOccurs = new List<Occur>();

                // bacteria
                if (data.Bacteria != null)
                {
                    var bacteriaOccurrences = db.Occurrences.Include(o=>o.Bacteria).Where(
                        o => o.District.ToLower().Trim() == region.Name.ToLower().Trim() &&
                        DateTime.Compare(o.EndDate, startDate) >= 0 &&
                        DateTime.Compare(o.EndDate, endDate) <= 0 &&
                        o.Bacteria != null && o.Infection == null && data.Bacteria.Contains(o.Bacteria.ID.ToString())).OrderBy(o => o.Bacteria.ID).ToList();


                    foreach (Occurrence occurrence in bacteriaOccurrences)
                    {
                        if (occurrence.Bacteria.ID != previous)
                        {//occurrence != previous) {
                            if (!infData.IsNull)
                            { // meaning that current occurrence does not have same bacteria as previous one
                                infData.Occurrences = occurs;
                                infData.OccurrencesOriginal = copyOccurs;
                                infBactDatas.Add(infData); // and it is not null, so must be added to the list
                            }
                            infData = new InfBactData();
                            occurs = new List<Occur>();
                            infData.IsBacteria = true;
                            infData.ID = occurrence.Bacteria.ID;
                            infData.IsNull = false;
                            Bacteria bacteria = db.Bacteria
                                .Include(item => item.Attachments)
                                .Include(item => item.Infections)
                                .Where(o => o.ID == occurrence.Bacteria.ID).First();
                            infData.Name = bacteria.Name;
                            infData.BactStrain = bacteria.Strain;
                            infData.BactIntroduction = bacteria.Introduction;
                            infData.BactTaxonomy = bacteria.Taxonomy;
                            infData.Definition = bacteria.Introduction;
                            infData.BactGramStaining = bacteria.GramStaining;
                            infData.BactShape = bacteria.Shape;
                            infData.BactOxygenRequirement = bacteria.OxygenRequirement;
                            infData.BactClinicalCharacteristics = bacteria.ClinicalCharacteristics;
                            infData.BactOccurrence = bacteria.Occurence;
                            infData.BactInvolvingDiseases = bacteria.InvolingDiseases;
                            infData.BactNCBI = bacteria.NCBI;
                            infData.BactAerobicity = bacteria.Aerobicity;
                            infData.BactMorphology = bacteria.Morphology;
                            infData.BactVirulenceFactors = bacteria.VirulenceFactors;
                            infData.BactLikeness = bacteria.Likeness;
                            infData.Treatment = bacteria.Treatment;
                            infData.BactInteractions = bacteria.InteractionWithOrganism;
                            infData.ModeofTransmissions = bacteria.ModeOfTransmission;
                            infData.References = bacteria.References;
                            infData.Adaptability = bacteria.Adaptability;
                            infData.Environmental = bacteria.Enviromental;
                            infData.District = bacteria.District;
                            infData.Genetics = bacteria.Genetics;
                            infData.AgeGroup = bacteria.AgeGroup;
                            infData.References = bacteria.References;
                            infData.InfHistory = bacteria.History;
                            List<KeyValue> infections = new List<KeyValue>();
                            foreach (var infection in bacteria.Infections)
                            {
                                infections.Add(new KeyValue() { Key = infection.Name, Value = infection.ID.ToString() });
                            }
                            List<KeyValue> attachments = new List<KeyValue>();
                            foreach (var attachment in bacteria.Attachments)
                            {
                                attachments.Add(new KeyValue() { Key = attachment.Name, Value = attachment.Link });
                            }
                            infData.BactInfections = infections;
                            infData.Attachments = attachments;
                        }
                        Occur occur = new Occur()
                        {
                            StartDate = occurrence.StartDate,
                            EndDate = occurrence.EndDate.ToString(),
                            Latitude = occurrence.Latitude,
                            Longitude = occurrence.Longitude,
                            DeathRate = occurrence.DeathRate,
                            Prevalence = occurrence.Prevalence,
                            Location = occurrence.ReportedLocation
                        };
                        Occur copyOfOccur = new Occur()
                        {
                            StartDate = occurrence.StartDate,
                            EndDate = occurrence.EndDate.ToString("yyyy-MM"),
                            Latitude = occurrence.Latitude,
                            Longitude = occurrence.Longitude,
                            DeathRate = occurrence.DeathRate,
                            Prevalence = occurrence.Prevalence,
                            Location = occurrence.ReportedLocation
                        };
                        copyOccurs.Add(copyOfOccur);
                        occurs.Add(occur);

                        previous = occurrence.Bacteria.ID;
                    }
                    if (!infData.IsNull)
                    { // meaning that current occurrence does not have same bacteria as previous one
                        infData.Occurrences = occurs;
                        infData.OccurrencesOriginal = copyOccurs;
                        infBactDatas.Add(infData); // and it is not null, so must be added to the list
                    }
                }

                if (data.Infections != null)
                {
                    var infectionOccurrences = db.Occurrences.Include(o=>o.Infection).Where(
                        o => o.District.ToLower().Trim() == region.Name.ToLower().Trim() &&
                        DateTime.Compare(o.EndDate, startDate) >= 0 &&
                        DateTime.Compare(o.EndDate, endDate) <= 0 &&
                        o.Infection != null && o.Bacteria == null && data.Infections.Contains(o.Infection.ID.ToString())).OrderBy(o => o.Infection.ID).ToList();

                    infData = new InfBactData();
                    previous = -1;
                    occurs = new List<Occur>();
                    copyOccurs = new List<Occur>();
                    foreach (Occurrence occurrence in infectionOccurrences)
                    {
                        if (occurrence.Infection.ID != previous)
                        {//occurrence != previous) {
                            if (!infData.IsNull)
                            { // meaning that current occurrence does not have same bacteria as previous one
                                infData.Occurrences = occurs;
                                infData.OccurrencesOriginal = copyOccurs;
                                infBactDatas.Add(infData); // and it is not null, so must be added to the list
                            }
                            infData = new InfBactData();
                            occurs = new List<Occur>();
                            infData.IsBacteria = false;
                            infData.ID = occurrence.Infection.ID;
                            infData.IsNull = false;
                            Infection infection = db.Infections
                                .Include(item => item.Attachments)
                                .Include(item => item.Bacterias)
                                .Include(item => item.ClinicalInformation)
                                .Where(o => o.ID == occurrence.Infection.ID).First();
                            infData.Name = infection.Name;
                            infData.InfType = infection.Type;
                            infData.Definition = infection.Definition;
                            infData.InfClassification = infection.Classification;
                            infData.InfSymptoms = infection.Symptoms;
                            infData.InfSeasonal = infection.Seasonal;
                            infData.InfPathophysiology = infection.Pathophysiology;
                            infData.ModeofTransmissions = infection.ModeOfTransmission;
                            infData.InfDiagnosis = infection.Diagnosis;
                            infData.InfPrevention = infection.Prevention;
                            infData.Treatment = infection.Treatment;
                            infData.InfEpidemiology = infection.Epidimology;
                            infData.References = infection.References;
                            infData.InfHistory = infection.History;
                            infData.Adaptability = infection.Adaptability;
                            infData.Environmental = infection.Enviromental;
                            infData.District = infection.District;
                            infData.Genetics = infection.Genetics;
                            infData.AgeGroup = infection.AgeGroup;

                            List<KeyValue> ClinicalInfos = new List<KeyValue>();
                            foreach (var ci in infection.ClinicalInformation)
                            {
                                ClinicalInfos.Add(new KeyValue() { Key = ci.ClinicName, Value = ci.ID.ToString() });
                            }

                            List<KeyValue> Bacterias = new List<KeyValue>();
                            foreach (var bacteria in infection.Bacterias)
                            {
                                Bacterias.Add(new KeyValue() { Key = bacteria.Name, Value = bacteria.ID.ToString() });
                            }
                            List<KeyValue> attachments = new List<KeyValue>();
                            foreach (var attachment in infection.Attachments)
                            {
                                attachments.Add(new KeyValue() { Key = attachment.Name, Value = attachment.Link });
                            }
                            infData.InfBacteriaList = Bacterias;
                            infData.Attachments = attachments;
                            infData.InfClinicalInformationList = ClinicalInfos;
                        }
                        Occur occur = new Occur()
                        {
                            StartDate = occurrence.StartDate,
                            EndDate = occurrence.EndDate.ToString(),
                            Latitude = occurrence.Latitude,
                            Longitude = occurrence.Longitude,
                            DeathRate = occurrence.DeathRate,
                            Prevalence = occurrence.Prevalence,
                            Location = occurrence.ReportedLocation
                        };
                        Occur copyOfOccur = new Occur()
                        {
                            StartDate = occurrence.StartDate,
                            EndDate = occurrence.EndDate.ToString("yyyy-MM"),
                            Latitude = occurrence.Latitude,
                            Longitude = occurrence.Longitude,
                            DeathRate = occurrence.DeathRate,
                            Prevalence = occurrence.Prevalence,
                            Location = occurrence.ReportedLocation
                        };
                        copyOccurs.Add(copyOfOccur);
                        occurs.Add(occur);
                        previous = occurrence.Infection.ID;
                    }
                    if (!infData.IsNull)
                    { // meaning that current occurrence does not have same bacteria as previous one
                        infData.Occurrences = occurs;
                        infData.OccurrencesOriginal = copyOccurs;
                        infBactDatas.Add(infData); // and it is not null, so must be added to the list
                    }
                }

                // calculating total prevs and total dts
                //foreach (InfBactData ibd in infBactDatas) {
                //    long totalPrevs = 0;
                //    long totalDts = 0;
                //    var count = ibd.Occurrences.Count;
                //    var ocs = ibd.Occurrences;
                //    for (int i = 0; i < ocs.Count; i++) {
                //        for (int j = i + 1; j < ocs.Count; j++) {
                //            if (ibd.Occurrences[i].Latitude == ibd.Occurrences[j].Latitude && ibd.Occurrences[i].Longitude == ibd.Occurrences[j].Longitude)
                //            {
                //                ibd.Occurrences[i].Prevalence += ibd.Occurrences[j].Prevalence;
                //                ibd.Occurrences[i].DeathRate += ibd.Occurrences[j].DeathRate;
                //                ibd.Occurrences.RemoveAt(j);
                //                count--;
                //            }
                //        }
                //        //if (i + 1 < ibd.Occurrences.Count && ibd.Occurrences[i].Latitude == ibd.Occurrences[i + 1].Latitude && ibd.Occurrences[i].Longitude == ibd.Occurrences[i + 1].Longitude)
                //        //{
                //        //    ibd.Occurrences[i].Prevalence += ibd.Occurrences[i + 1].Prevalence;
                //        //    ibd.Occurrences[i].DeathRate += ibd.Occurrences[i + 1].DeathRate;
                //        //    ibd.Occurrences.RemoveAt(i + 1);
                //        //    count--;
                //        //}
                //        totalPrevs += ibd.Occurrences[i].Prevalence;
                //        totalDts += ibd.Occurrences[i].DeathRate;
                //        prevGrandTotal = ibd.Occurrences[i].Prevalence > prevGrandTotal ? ibd.Occurrences[i].Prevalence : prevGrandTotal;
                //        dtGrandTotal = ibd.Occurrences[i].DeathRate > dtGrandTotal ? ibd.Occurrences[i].DeathRate : dtGrandTotal;
                //    }
                //    ibd.TotalPrevalences = totalPrevs;
                //    ibd.TotalDeathRates = totalDts;
                //    //prevGrandTotal += totalPrevs;
                //    //dtGrandTotal += totalDts;
                //}


                foreach (InfBactData ibd in infBactDatas)
                {
                    long totalPrevs = 0;
                    long totalDts = 0;
                    List<Occur> newList = new List<Occur>();
                    for (int i = 0; i < ibd.Occurrences.Count; i++)
                    {
                        bool found = false;
                        for (int j = 0; j < newList.Count; j++)
                        {
                            if (ibd.Occurrences[i].Latitude == newList[j].Latitude && ibd.Occurrences[i].Longitude == newList[j].Longitude)
                            {
                                newList[j].Prevalence += ibd.Occurrences[i].Prevalence;
                                newList[j].DeathRate += ibd.Occurrences[i].DeathRate;
                                newList[j].StartDate = ibd.Occurrences[i].StartDate.CompareTo(newList[j].StartDate) < 0 ? ibd.Occurrences[i].StartDate : newList[j].StartDate;
                                newList[j].EndDate = ibd.Occurrences[i].EndDate.CompareTo(newList[j].EndDate) > 0 ? ibd.Occurrences[i].EndDate : newList[j].EndDate;
                                found = true;
                            }
                        }
                        if (!found)
                        {
                            Occur newOccur = new Occur();
                            newOccur.DeathRate = ibd.Occurrences[i].DeathRate;
                            newOccur.Latitude = ibd.Occurrences[i].Latitude;
                            newOccur.Longitude = ibd.Occurrences[i].Longitude;
                            newOccur.Prevalence = ibd.Occurrences[i].Prevalence;
                            newOccur.StartDate = ibd.Occurrences[i].StartDate;
                            newOccur.EndDate = ibd.Occurrences[i].EndDate;
                            newOccur.Location = ibd.Occurrences[i].Location;
                            newList.Add(newOccur);
                        }
                        totalPrevs += ibd.Occurrences[i].Prevalence;
                        totalDts += ibd.Occurrences[i].DeathRate;
                        prevGrandTotal = ibd.Occurrences[i].Prevalence > prevGrandTotal ? ibd.Occurrences[i].Prevalence : prevGrandTotal;
                        dtGrandTotal = ibd.Occurrences[i].DeathRate > dtGrandTotal ? ibd.Occurrences[i].DeathRate : dtGrandTotal;
                    }
                    ibd.TotalPrevalences = totalPrevs;
                    ibd.TotalDeathRates = totalDts;
                    ibd.Occurrences = newList;
                }



                regionData.Results = infBactDatas;
                returnDatas.Add(regionData);
            }
            returnDatas.Add(countriesGeo.Distinct());
            returnDatas.Add(prevGrandTotal);
            returnDatas.Add(dtGrandTotal);
            return Json(returnDatas);
        }


        /// <summary>
        ///  Method to search region by country name
        /// </summary>
        /// <param name="id">Country Name</param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Region> SearchRegion(string id)
        {
            string searchString = id;

            var regions = from s in db.Regions select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                regions = regions.Where(s => s.Country.Name.Contains(searchString)).OrderBy(r => r.Name);
            }
            return regions.ToList();
        }

        /// <summary>
        /// Method to search infections and bacteria by name or part of name
        /// </summary>
        /// <param name="id">Name or part of the name of an infection or bacteria</param>
        /// <returns></returns>
        [HttpGet]
        public List<Object> SearchInfection(string id)
        {

            string searchString = id == null || id == "undefined" ? "" : id;

            var infections = from s in db.Infections select s;
            var bacteria = from t in db.Bacteria select t;
            if (!String.IsNullOrEmpty(searchString))
            {
                infections = infections.Where(s => s.Name.Contains(searchString));
                bacteria = bacteria.Where(t => t.Name.Contains(searchString));
            }
            List<Object> names = new List<Object>();
            foreach (var inf in infections)
            {
                names.Add(inf);
            }
            foreach (var bact in bacteria)
            {
                names.Add(bact);
            }
            return names;
        }

        /// <summary>
        /// Method to eturn a list of all the infections in the database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Infection> GetAllInfections()
        {
            return db.Infections.ToList();
        }

        /// <summary>
        /// Method to return the results based on the general search term passed into the method
        /// </summary>
        /// <param name="id">Name or part of the name of an infection or bacteria</param>
        /// <returns></returns>
        [HttpGet]
        public List<SearchResult> SearchDatabase(string query)
        {

            string searchString = String.IsNullOrEmpty(query) || query == "undefined" ? "" : query.ToLower();

            var infections = from s in db.Infections select s;
            var bacteria = from t in db.Bacteria select t;
            if (!String.IsNullOrEmpty(searchString))
            {
                infections = db.Infections.Where(b =>   b.Name.ToLower().Contains(searchString) ||
                                                        b.Name.ToLower() == searchString ||
                                                        b.ID.ToString().ToLower().Contains(searchString) ||
                                                        b.ID.ToString().ToLower() == searchString);
                bacteria = db.Bacteria.Where(b =>   b.Name.ToLower().Contains(searchString) ||
                                                    b.Name.ToLower() == searchString ||
                                                    b.ID.ToString().ToLower().Contains(searchString) ||
                                                    b.ID.ToString().ToLower() == searchString);
            }
            
            List<SearchResult> results = new List<SearchResult>();
            String definition = null;
            foreach (var inf in infections)
            {
                if (String.IsNullOrEmpty(inf.Definition) || string.IsNullOrWhiteSpace(inf.Definition))
                {
                    definition = "";
                }
                else {
                    if (inf.Definition.Length > 300) {
                        definition = inf.Definition.Substring(0, 300) + "...";
                    } else {
                        definition = inf.Definition;
                    }
                }
                results.Add(new SearchResult()
                {
                    Name = inf.Name,
                    Definition = definition,
                    Type = "Infection",
                    Link = "Infections/Details/" + inf.ID
                });
            }
            foreach (var bact in bacteria)
            {
                if (String.IsNullOrEmpty(bact.Introduction) || string.IsNullOrWhiteSpace(bact.Introduction))
                {
                    definition = "";
                }
                else
                {
                    if (bact.Introduction.Length > 300)
                    {
                        definition = bact.Introduction.Substring(0, 300) + "...";
                    }
                    else
                    {
                        definition = bact.Introduction;
                    }
                }
                results.Add(new SearchResult()
                {
                    Name = bact.Name,
                    Definition = definition,
                    Type = "Bacteria",
                    Link = "Bacteria/Details/" + bact.ID
                }); 
            }
            return results;
        }

        ///////
        ///
        [HttpPost]
        public IHttpActionResult UploadEditorFile()
        {

            var subPath = "Attachments/Articles/" + "/";
        
            bool exists = Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/" + subPath));
            if (!exists)
            {
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/" + subPath));
            }

            if (HttpContext.Current.Request.Files == null || HttpContext.Current.Request.Files.Count <= 0) {
                return null;
            }

            //for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
            //{
                HttpPostedFile hpf = HttpContext.Current.Request.Files[0];
                var fileName = Path.GetFileName(hpf.FileName);
                if (string.IsNullOrEmpty(fileName))
                {
                    return null ;
                }

                var path = Path.Combine(subPath, fileName);
                hpf.SaveAs(HttpContext.Current.Server.MapPath("~/" + path));

            //}

            return Ok(new { location = Url.Content("~/" + path) });
        }

        [HttpPost]
        public IHttpActionResult jwt() {
            string strToken = "-----BEGIN PRIVATE KEY-----" +
                "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDM4TI1lkpp4BUu"+
                "dth2sbb861bSa8k/nN+4/lm0TM8bNeaWd+slRvJrA5/U0pjFcubEg1/G1oYN3pQk"+
                "aUd/mTHXajvG2FH+Jkn4k0fFH6ugXdYlK1u13U6pKf7tiB4o4QnLBO0I773kPLF2"+
                "RO2X04q5kPZEDEgd20dxM34iU6o5rBXGGKvwfVvW5qVguIiM643+rlj4TM3xcTGN"+
                "TDQnZIZNd2/ZepZgJ3ygf/CWQTyaNRABevO9PBjkMDNQ6Y58Nta8mfUpGqILWayN"+
                "fU2QYPzwQacd0+WjaVEooLNXl67fyxshKrhOIRPd/o3O2ajLeFlDYt7BmtFW9qrH"+
                "LqgGxLUnAgMBAAECggEBAMmee/i8M/LJME8Paao07W0sc76Tj/LoAEpfFAwY4l9W"+
                "xdshm+iNTxb89BdOQEVe87ZDysc0aavQdFz4tgHd0my5AKaITvga2gSje6eDqTQh"+
                "5BGj/5aTeUuEJnm+0F9ORoLiEBoRRAANsl4/KsmUxhojjBYfOAEZv86o6+IAD667"+
                "5NUwYYsEiwsHRwTa37tgAWBmV/d14F/c1CaEnNVbQUbxASaZ+rz37eYf89QFl0CA"+
                "TVDLe4AO4HxuYMhWh037TsSmOyX2ZL0yjmuvureD+JZwxQgbGNmw5Lpdk2iXFskM"+
                "yGfTEa3rSzkChXUecFY+JKaLjFjMyxNjBcmPSipDjcECgYEA9Oclpkg9KzOWRW0K"+
                "JS/UCjqhGANCVorl/gyPDLl+wD37iB0v7NIlyZTCSeeAg6n7HmuYt4YBp7tq1/JQ"+
                "3how5DzStkF+WuukgVCU20dlAySYafToT9lrqO4P/sZ2L8QIW/BDtiL0L5Y/3ysJ"+
                "5xRmTZTDp5JjN6Bqrjoe8Y4SxNcCgYEA1inIZ3TtMsZxCNb5pyCp0H/6dBFFh/pH"+
                "UzZ2DrE9y7RCTzXcWzxGW6PrNizWglfqcCf4DLZVbHA/bQOzZ3rGE8dHRjk023fs"+
                "8qhdrxMsQjgI/3TksMUFsID9C8Xr33fV8+jgXaBzQ5NSkBfWHiahp1FrqG/XgLCD"+
                "VCL62dUSODECgYEA8LLuEgwV8ov1Oa79tabYZGVTR2KMpLpHafW18u+tYAyLVzA4"+
                "li3E7ebDPhfF+6HpKfDeXCHdJpnRXQTP7jsXqnsNLgwYwAux11b3trzozrn9Asau"+
                "PSyX36R7HBC6wB+Ph60RH8Yi8VJe3FxUk0U9dyqrUdtSoK2NrEd7ejYExGkCgYEA"+
                "t4LUG3qA6hpKr1VuVTxS5rtTA/PszaizR2WKYeqUViHfE/rGGFOcwlfCbAidwtDM"+
                "rVBCZ7oxlprUbnhWWVxFsJr4wv6wAUNRAVkt9aALM67KXwNuFt6skmLk8hqllDYe"+
                "WkN5RiD/w67Qd3JFYk6eBD5rbshvM/GMhyL2KoQ+eXECgYEAgmHV1jTlzykUvDQ+"+
                "jRjSyW2TbY+h88+tnkvUIIoDeYcnovas+1NU/OHp+OExlu/MjlJfVu+tmTYMYvG9"+
                "3JLcvoPYGCoLrEZgfbx6ML9Wm3gXndPcZshECHLDZ1pY1dc81f0DLi2NQGRnm/Vy"+
                "a3KWQCkqRXOthSUKuHCKUnNBdcs=" +
                "-----END PRIVATE KEY-----";
            return Ok(new { token = strToken });
        }

        [HttpPost]
        public Boolean CreateUpdateArticle( Article article) {

            if (article.ID == -1) {
                Article art = new Article() {
                    Content = article.Content,
                    PublishDate = article.PublishDate,
                    SequenceNumber = article.SequenceNumber,
                    Title = article.Title,
                    User_ID = "-1"
                };
                db.Articles.Add(art);
            }
            else {
                Article art = db.Articles.Where(a => a.ID == article.ID).First();
                if (art == null)
                {
                    return false;
                }
                art.PublishDate = article.PublishDate;
                art.Title = article.Title;
                art.SequenceNumber = article.SequenceNumber;
                art.Content = article.Content;
                
            }
            db.SaveChanges();
            return true;
        }

        [HttpPost]
        public Boolean DeleteArticle(string id) {

            return true;
        }

        [HttpGet]
        public IHttpActionResult GetArticle(int id) {
            var article = db.Articles.Find(id);
            if (article == null) {
                return null;
            }

            return Ok(new { article =  article});
        }

        [HttpGet]
        public IHttpActionResult GetFeaturedItem(int id)
        {
            var item = db.FeaturedItems.Find(id);
            if (item == null)
            {
                return null;
            }

            return Ok(new { featured = item });
        }

        [HttpPost]
        public IHttpActionResult UploadFeaturedImage()
        {

            var subPath = "Attachments/FeaturedItems/" + "/";

            bool exists = Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/" + subPath));
            if (!exists)
            {
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/" + subPath));
            }

            if (HttpContext.Current.Request.Files == null || HttpContext.Current.Request.Files.Count <= 0)
            {
                return Ok(new { location = "" });
            }

            //for (int i = 0; i < HttpContext.Current.Request.Files.Count; i++)
            //{
            HttpPostedFile hpf = HttpContext.Current.Request.Files[0];
            var fileName = Path.GetFileName(hpf.FileName);
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "mdrip-" + DateTime.Now.ToString();
            }

            var path = Path.Combine(subPath, fileName);
            hpf.SaveAs(HttpContext.Current.Server.MapPath("~/" + path));

            //}

            return Ok(new { location = Url.Content("~/" + path) });
        }

        [HttpPost]
        public Boolean CreateUpdateFeatured(FeaturedItem item)
        {

            if (item.ID == -1)
            {
                FeaturedItem f = new FeaturedItem()
                {
                    Title = item.Title,
                    DisplayText = item.DisplayText,
                    PublishDate = item.PublishDate,
                    SequenceNumber = item.SequenceNumber,
                    ImagePath = item.ImagePath,
                    Link = item.Link,
                    User_ID = "-1"
                };
                db.FeaturedItems.Add(f);
            }
            else
            {
                FeaturedItem f = db.FeaturedItems.Where(a => a.ID == item.ID).First();
                if (f == null)
                {
                    return false;
                }
                f.Title = item.Title;
                f.DisplayText = item.DisplayText;
                f.PublishDate = item.PublishDate;
                f.SequenceNumber = item.SequenceNumber;
                //f.ImagePath = item.ImagePath;
                f.Link = item.Link;
                f.User_ID = "-1";

            }
            db.SaveChanges();
            return true;
        }
    }
}