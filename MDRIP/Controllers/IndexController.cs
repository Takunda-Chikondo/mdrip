﻿using MDRIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MDRIP.Controllers
{
    public class IndexController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Home()
        {
            //if (User.Identity.IsAuthenticated)
            //{
            var alerts = db.Alerts.OrderBy(d => d.Date).Where(a => a.Date.CompareTo(DateTime.Today) <= 0).ToList();
            var articles = db.Articles.OrderBy(a => a.SequenceNumber).ThenBy(a => a.Title)
                .Where(a => a.PublishDate.CompareTo(DateTime.Today) <= 0).ToList();

            var featured = db.FeaturedItems.OrderBy(a => a.SequenceNumber).ThenBy(a => a.Title)
                .Where(a => a.PublishDate.CompareTo(DateTime.Today) <= 0).ToList();

            LandingViewModel landingViewModel = new LandingViewModel();
            landingViewModel.Alerts = alerts;
            landingViewModel.Articles = articles;
            landingViewModel.FeaturedItems = featured;

            return View(landingViewModel);
        }
    }
}