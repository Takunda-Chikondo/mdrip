﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MDRIP.Models;

namespace MDRIP.Controllers
{
    [Authorize]
    public class InfectionAttachmentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: InfectionAttachments
        public ActionResult Index()
        {
            return View(db.InfectionAttachments.ToList());
        }

        // GET: InfectionAttachments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfectionAttachment infectionAttachment = db.InfectionAttachments.Find(id);
            if (infectionAttachment == null)
            {
                return HttpNotFound();
            }
            return View(infectionAttachment);
        }

        // GET: InfectionAttachments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InfectionAttachments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Link")] InfectionAttachment infectionAttachment)
        {
            if (ModelState.IsValid)
            {
                db.InfectionAttachments.Add(infectionAttachment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(infectionAttachment);
        }

        // GET: InfectionAttachments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfectionAttachment infectionAttachment = db.InfectionAttachments.Find(id);
            if (infectionAttachment == null)
            {
                return HttpNotFound();
            }
            return View(infectionAttachment);
        }

        // POST: InfectionAttachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Link")] InfectionAttachment infectionAttachment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(infectionAttachment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(infectionAttachment);
        }

        // GET: InfectionAttachments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfectionAttachment infectionAttachment = db.InfectionAttachments.Find(id);
            if (infectionAttachment == null)
            {
                return HttpNotFound();
            }
            return View(infectionAttachment);
        }

        // POST: InfectionAttachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InfectionAttachment infectionAttachment = db.InfectionAttachments.Find(id);
            db.InfectionAttachments.Remove(infectionAttachment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteAttachment(int id)
        {
            InfectionAttachment infectionAttachment = db.InfectionAttachments.Find(id);

            var path = Request.MapPath("~/" + infectionAttachment.Link);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            db.InfectionAttachments.Remove(infectionAttachment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
