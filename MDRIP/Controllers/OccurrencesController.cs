﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MDRIP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MDRIP.Controllers
{
    [Authorize]
    public class OccurrencesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        #region UserManager
        private ApplicationUserManager _userManager;

        public OccurrencesController()
        {
        }
        public OccurrencesController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

		// GET: Occurrences
		public ActionResult Index(string searchString)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            var occurrence = db.Occurrences.Include(x=>x.Bacteria).Include(x => x.Infection).Include(x => x.Attachments);
            var occurrences = occurrence;
            if (!String.IsNullOrEmpty(searchString))
            {
                occurrences = occurrence.Where(s => s.ReportedLocation.ToLower().Contains(searchString.ToLower()));
                occurrences = occurrences.Union(occurrence.Where(s => s.District.ToLower().Contains(searchString.ToLower())));
                occurrences = occurrences.Union(occurrence.Where(s => s.Bacteria.Name != null && s.Bacteria.Name.ToLower().Contains(searchString.ToLower())));
                occurrences = occurrences.Union(occurrence.Where(s => s.Infection.Name != null && s.Infection.Name.ToLower().Contains(searchString.ToLower())));
                occurrences = occurrences.Union(occurrence.Where(s => s.Attachments.Count > 0 && s.Attachments.Any(x=> x.Name.ToLower().Contains(searchString.ToLower()))));
            }

            if (User.IsInRole("ResearcherScientist"))
            {
                return View(occurrences.Where(i => i.User_ID == user.Id).Include(i=>i.Bacteria).Include(i=> i.Infection).Include(i => i.Attachments).ToList());
            }
            else
            {
                return View(occurrences.Include(i => i.Bacteria).Include(i => i.Infection).Include(i=>i.Attachments).ToList());
            }
        }

        // GET: Occurrences/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Occurrence occurrence = db.Occurrences
                .Include(x=>x.Infection)
                .Include(x => x.Bacteria)
                .Include(x => x.Attachments)
                .Where(x=>x.ID == id).SingleOrDefault();

            if (occurrence == null)
            {
                return HttpNotFound();
            }
            
            return View(occurrence);
        }

        // GET: Occurrences/Create
        public ActionResult Create() {

            var bact = db.Bacteria.OrderBy(x=>x.Name).ToList();
            var inf = db.Infections.OrderBy(x => x.Name).ToList();
            var lst = new List<InfectionBacteria>();
            bact.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Bacteria - " + x.Name, Type="Bacteria" }));
            inf.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Infection - " + x.Name, Type = "Infection" }));

            var OccurencesViewModel = new OccurencesViewModel
            {
                Bacteria = db.Bacteria.ToList(),
                Infection = db.Infections.ToList(),
                InfsBacts = lst,
                Regions = db.Regions.Include(r => r.Country).OrderBy(r => r.Country.Name).ThenBy(r => r.Name).ToList()
			};
            return View(OccurencesViewModel);
        }

        // POST: Occurrences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OccurencesViewModel model)
        {


            //if (ModelState.IsValid)
            //{

                // Model validation
                if (string.IsNullOrWhiteSpace(model.InfectionBacteriaID) ||
                    string.IsNullOrWhiteSpace(model.Occurence.ReportedLocation) ||
                    string.IsNullOrWhiteSpace(model.Occurence.District) ||
                    model.Occurence.StartDate == null) {

                var bact = db.Bacteria.OrderBy(x => x.Name).ToList();
                var inf = db.Infections.OrderBy(x => x.Name).ToList();
                var lst = new List<InfectionBacteria>();
                bact.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Bacteria - " + x.Name, Type = "Bacteria" }));
                inf.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Infection - " + x.Name, Type = "Infection" }));

                var OccurencesViewModel = new OccurencesViewModel
                {
                    Bacteria = db.Bacteria.ToList(),
                    Infection = db.Infections.ToList(),
                    InfsBacts = lst,
                    Regions = db.Regions.Include(r => r.Country).OrderBy(r => r.Country.Name).ThenBy(r => r.Name).ToList(),
                    Occurence = model.Occurence,
                    InfectionBacteriaID = model.InfectionBacteriaID
                };
                return View(OccurencesViewModel);

            } else { 

                string infectionPrefix = "Infection - ";
                string bacteriaPrefix = "Bacteria - ";

                if (model.InfectionBacteriaID.Contains(infectionPrefix)) { // infection
                    model.InfectionID = Convert.ToInt32(model.InfectionBacteriaID.Substring(infectionPrefix.Length, model.InfectionBacteriaID.Length - infectionPrefix.Length));
                    model.Occurence.Infection = db.Infections.Where(u => u.ID == model.InfectionID).SingleOrDefault();
                }
                else
                {
                    model.BacteriaID = Convert.ToInt32(model.InfectionBacteriaID.Substring(bacteriaPrefix.Length, model.InfectionBacteriaID.Length - bacteriaPrefix.Length));
                    model.Occurence.Bacteria = db.Bacteria.Where(u => u.ID == model.BacteriaID).SingleOrDefault();
                }

                model.Occurence.User_ID = User.Identity.GetUserId();

                if (model.Occurence.ID > 0)
                {
                    db.Entry(model.Occurence).State = EntityState.Modified;
                }
                else
                {
                    db.Occurrences.Add(model.Occurence);
                }
                db.SaveChanges();

                SaveAttachments(model.Occurence);

                return RedirectToAction("Index");
            }    

        }

        private void SaveAttachments(Occurrence occurrence)
        {
            var subPath = "Attachments/Occurrences/" + occurrence.ID + "/";
            bool exists = Directory.Exists(Server.MapPath("~/" + subPath));
            if (!exists)
            {
                Directory.CreateDirectory(Server.MapPath("~/" + subPath));
            }
            for (var i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase hpf = Request.Files[i];
                var fileName = Path.GetFileName(hpf.FileName);
                if (fileName == null || fileName == "")
                {
                    break;
                }

                var path = Path.Combine(subPath, fileName);
                hpf.SaveAs(Server.MapPath("~/" + path));
                OccurrenceAttachment a = new OccurrenceAttachment()
                {
                    Name = fileName,
                    Occurrence = occurrence,
                    Link = path
                };
                db.OccurrenceAttachments.Add(a);
                db.SaveChanges();
            }
        }

        // GET: Occurrences/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Occurrence occurrence = db.Occurrences
                .Include(x => x.Infection)
                .Include(x => x.Bacteria)
                .Include(x => x.Attachments)
                .Where(x => x.ID == id).SingleOrDefault();

            if (occurrence == null)
            {
                return HttpNotFound();
            }

            var bact = db.Bacteria.OrderBy(x => x.Name).ToList();
            var inf = db.Infections.OrderBy(x => x.Name).ToList();
            var lst = new List<InfectionBacteria>();
            bact.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Bacteria - " + x.Name, Type = "Bacteria" }));
            inf.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Infection - " + x.Name, Type = "Infection" }));

            string infectionBacteriaID = "";
            if (occurrence.Bacteria != null)
            {
                infectionBacteriaID = "Bacteria - " + occurrence.Bacteria.ID;
            }
            else {
                infectionBacteriaID = "Infection - " + occurrence.Infection.ID;
            }

            var OccurencesViewModel = new OccurencesViewModel
            {
                Bacteria = db.Bacteria.ToList(),
                Infection = db.Infections.ToList(),
                InfsBacts = lst,
                Regions = db.Regions.Include(r => r.Country).OrderBy(r => r.Country.Name).ThenBy(r => r.Name).ToList(),
                Occurence = occurrence,
                InfectionBacteriaID = infectionBacteriaID
            };

            return View(OccurencesViewModel);
        }

        // POST: Occurrences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OccurencesViewModel model)
        {
            if (ModelState.IsValid)
            {
				Occurrence occurrence = model.Occurence;
                db.Entry(occurrence).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

			var id = model.Occurence.ID;
			OccurencesViewModel occurrenceModel = new OccurencesViewModel
			{
				Regions = db.Regions.Include(r => r.Country).OrderBy(r => r.Country.Name).ThenBy(r => r.Name),
				Occurence = db.Occurrences.Find(id),
			};
            //return View(occurrenceModel);


            if (string.IsNullOrWhiteSpace(model.InfectionBacteriaID) ||
                    string.IsNullOrWhiteSpace(model.Occurence.ReportedLocation) ||
                    string.IsNullOrWhiteSpace(model.Occurence.District) ||
                    model.Occurence.StartDate == null)
            {

                var bact = db.Bacteria.OrderBy(x => x.Name).ToList();
                var inf = db.Infections.OrderBy(x => x.Name).ToList();
                var lst = new List<InfectionBacteria>();
                bact.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Bacteria - " + x.Name, Type = "Bacteria" }));
                inf.ForEach(x => lst.Add(new InfectionBacteria() { ID = x.ID, Name = "Infection - " + x.Name, Type = "Infection" }));

                var OccurencesViewModel = new OccurencesViewModel
                {
                    Bacteria = db.Bacteria.ToList(),
                    Infection = db.Infections.ToList(),
                    InfsBacts = lst,
                    Regions = db.Regions.Include(r => r.Country).OrderBy(r => r.Country.Name).ThenBy(r => r.Name).ToList(),
                    Occurence = model.Occurence,
                    InfectionBacteriaID = model.InfectionBacteriaID
                };
                return View(OccurencesViewModel);

            }
            else
            {

                string infectionPrefix = "Infection - ";
                string bacteriaPrefix = "Bacteria - ";

                if (model.InfectionBacteriaID.Contains(infectionPrefix))
                { // infection
                    model.InfectionID = Convert.ToInt32(model.InfectionBacteriaID.Substring(infectionPrefix.Length, model.InfectionBacteriaID.Length - infectionPrefix.Length));
                    model.Occurence.Infection = db.Infections.Where(u => u.ID == model.InfectionID).SingleOrDefault();
                }
                else
                {
                    model.BacteriaID = Convert.ToInt32(model.InfectionBacteriaID.Substring(bacteriaPrefix.Length, model.InfectionBacteriaID.Length - bacteriaPrefix.Length));
                    model.Occurence.Bacteria = db.Bacteria.Where(u => u.ID == model.BacteriaID).SingleOrDefault();
                }

                model.Occurence.User_ID = User.Identity.GetUserId();
                db.Occurrences.Add(model.Occurence);
                db.SaveChanges();

                SaveAttachments(model.Occurence);

                return RedirectToAction("Index");
            }

        }

        // GET: Occurrences/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Occurrence occurrence = db.Occurrences.Find(id);
            if (occurrence == null)
            {
                return HttpNotFound();
            }
            return View(occurrence);
        }

        // POST: Occurrences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Occurrence occurrence = db.Occurrences.Find(id);
            db.Occurrences.Remove(occurrence);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
	}
}

public class InfectionBacteria {
    public int ID { get; set; }
    public String Name { get; set; }
    public String Type { get; set; }
}



/*
 
     
@using (Html.BeginForm())
{
    @Html.AntiForgeryToken()
    <h4>Occurrence</h4>
    <hr />

    @Html.Label("Reported Location")
    <div class="col-md-10">
        <input type="text" id="txtautocomplete" placeholder="Enter the adddress" class="form-control" />
    </div>

    <div class="form-horizontal">
        @Html.ValidationSummary(true, "", new { @class = "text-danger" })
        @Html.HiddenFor(model => model.Occurence.ID)
        <div class="form-group">
            <div class="col-md-10">
                @Html.HiddenFor(model => model.Occurence.ReportedLocation, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Occurence.ReportedLocation, "", new { @class = "text-danger" })
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10">
                @Html.HiddenFor(model => model.Occurence.Latitude, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Occurence.Latitude, "", new { @class = "text-danger" })
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10">
                @Html.HiddenFor(model => model.Occurence.Longitude, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Occurence.Longitude, "", new { @class = "text-danger" })
            </div>
        </div>
        <div class="form-group">
            @Html.LabelFor(model => model.Occurence.District, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                <select class="form-control" id="Occurence_District" name="Occurence.District">
                    <option value="">Select Region/District</option>
                    @foreach (var reg in Model.Regions)
                    {
                        if (Model.Occurence.District == reg.Name)
                        {
                            <option value="@reg.Name" selected>@reg.Country.Name - @reg.Name</option>
                        }
                        else
                        {
                            <option value="@reg.Name">@reg.Country.Name - @reg.Name</option>
                        }
                    }
                </select>

                @*@Html.DropDownListFor(model => model.Occurence.District, new SelectList(Model.Regions, "id", "Name"), "Select Region/District", new { @class = "form-control" })*@
                @*@Html.HiddenFor(model => model.Occurence.District, new { htmlAttributes = new { @class = "form-control" } })*@
                @Html.ValidationMessageFor(model => model.Occurence.District, "", new { @class = "text-danger" })
            </div>
        </div>
        <div class="form-group">
            @Html.Label("StartDate", htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                <input class="form-control text-box single-line" data-val="true" data-val-date="The field StartDate must be a date." data-val-required="The StartDate field is required." id="StartDate" name="Occurence.StartDate" type="date" value="@Model.Occurence.StartDate.ToString("yyyy-MM-dd")">
            </div>
        </div>
        <div class="form-group">
            @Html.Label("EndDate", htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                <input class="form-control text-box single-line" data-val="true" data-val-date="The field EndDate must be a date." data-val-required="The EndDate field is required." id="EndDate" name="Occurence.EndDate" type="date" value="@Model.Occurence.EndDate.ToString("yyyy-MM-dd")">
            </div>
        </div>
        <div class="form-group">
            @Html.LabelFor(model => model.Occurence.Prevalence, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.Occurence.Prevalence, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Occurence.Prevalence, "", new { @class = "text-danger" })
            </div>
        </div>
        <div class="form-group">
            @Html.LabelFor(model => model.Occurence.DeathRate, htmlAttributes: new { @class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(model => model.Occurence.DeathRate, new { htmlAttributes = new { @class = "form-control" } })
                @Html.ValidationMessageFor(model => model.Occurence.DeathRate, "", new { @class = "text-danger" })
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Save" class="btn btn-default" />
            </div>
        </div>
    </div>
}
     
     */
