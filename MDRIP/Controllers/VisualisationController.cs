﻿using MDRIP.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace MDRIP.Controllers
{
    //[Authorize]
    public class VisualisationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Returns the index page with all the alerts that are currently active
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //if (User.Identity.IsAuthenticated)
            //{
            var alerts = db.Alerts.OrderBy(d => d.Date).Where(a=>a.Date.CompareTo(DateTime.Today) <=0).ToList();
            var articles = db.Articles.OrderBy(a => a.SequenceNumber).ThenBy(a => a.Title)
                .Where(a => a.PublishDate.CompareTo(DateTime.Today) <= 0).ToList();

            var featured = db.FeaturedItems.OrderBy(a => a.SequenceNumber).ThenBy(a => a.Title)
                .Where(a => a.PublishDate.CompareTo(DateTime.Today) <= 0).ToList();

            LandingViewModel landingViewModel = new LandingViewModel();
            landingViewModel.Alerts = alerts;
            landingViewModel.Articles = articles;
            landingViewModel.FeaturedItems = featured;    
                //from a in db.Articles
                //           orderby a.SequenceNumber
                //           where a.PublishDate.CompareTo(DateTime.Today) >= 0
                //           group a by a.SequenceNumber;

            //if (alerts == null || alerts.Count <= 0)
            //    {
            //        ViewBag.HasAlert = false;

            //    }
            //    else
            //    {
            //        ViewBag.HasAlert = true;
            //    }
                return View(landingViewModel);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Account");
            //}
        }

        /// <summary>
        /// Returns the visualisation page with an optional selected visualisation name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Visualisations(string id)
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.Id = id;
                var list = db.Countries.ToList();
                list.Sort((x, y) => x.Name.CompareTo(y.Name));
                return View(list);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}
