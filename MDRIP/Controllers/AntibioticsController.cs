﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MDRIP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MDRIP.Controllers
{
    [Authorize]
    public class AntibioticsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

		#region UserManager
		private ApplicationUserManager _userManager;

		public AntibioticsController()
		{
		}
		public AntibioticsController(ApplicationUserManager userManager)
		{
			UserManager = userManager;
		}

		public ApplicationUserManager UserManager
		{
			get
			{
				return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
			private set
			{
				_userManager = value;
			}
		}
		#endregion
		// GET: Antibiotics
		public ActionResult Index(string searchString)
        {
			{
                var user = UserManager.FindById(User.Identity.GetUserId());

                var antibiotic = from a in db.Antibiotics select a;
				if (!String.IsNullOrEmpty(searchString))
				{
					antibiotic = antibiotic.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
				}
                if (User.IsInRole("ResearcherScientist"))
                {
                    return View(antibiotic.Where(i => i.User_ID == user.Id).ToList());
                }
                else
                {
                    return View(antibiotic);
                }
			}
        }

        // GET: Antibiotics/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Antibiotic antibiotic = db.Antibiotics.Find(id);
            if (antibiotic == null)
            {
                return HttpNotFound();
            }
            return View(antibiotic);
        }

        // GET: Antibiotics/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Antibiotics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] Antibiotic antibiotic)
        {
            if (ModelState.IsValid)
            {
				antibiotic.User_ID = User.Identity.GetUserId();
				db.Antibiotics.Add(antibiotic);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(antibiotic);
        }

        // GET: Antibiotics/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Antibiotic antibiotic = db.Antibiotics.Find(id);
            if (antibiotic == null)
            {
                return HttpNotFound();
            }
            return View(antibiotic);
        }

        // POST: Antibiotics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Antibiotic antibiotic)
        {
            if (ModelState.IsValid)
            {
                db.Entry(antibiotic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(antibiotic);
        }

        // GET: Antibiotics/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Antibiotic antibiotic = db.Antibiotics.Find(id);
            if (antibiotic == null)
            {
                return HttpNotFound();
            }
            return View(antibiotic);
        }

        // POST: Antibiotics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Antibiotic antibiotic = db.Antibiotics.Find(id);
            db.Antibiotics.Remove(antibiotic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
