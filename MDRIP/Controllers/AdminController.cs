﻿using MailChimp.Net;
using MailChimp.Net.Core;
using MailChimp.Net.Models;
using MDRIP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MDRIP.Controllers
{
	[Authorize(Roles="Admin")]
    public class AdminController : Controller
    {
        ApplicationDbContext context;
        private UserManager<ApplicationUser> UserManager;

        private static readonly MailChimpManager Manager = new MailChimpManager("5c04e642468d6f45a3c515e3b7bb0c30-us18");

        public AdminController()
        {
            context = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
        }

        public ActionResult EditContent() {
            return View();
        }

        public async Task<ActionResult> Index()
        {

            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }

            var RolesList = context.Roles.ToList();
            var AccountsList = context.Users.ToList();
            var model = new AdminModel();
            var options = new ListRequest
            {
                Limit = 10
            };

            model.Accounts = AccountsList;
            model.Roles = RolesList;
            model.AccountRole = new AccountRoleAdminmodel();
            try
            {
                model.MailingLists = await Manager.Lists.GetAllAsync(options);
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }

            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }

            var articles = new List<KeyValuePair<int, string>>();
            foreach (var art in context.Articles.OrderBy(a=> a.Title).ToList()) {
                articles.Add(new KeyValuePair<int, string>(art.ID, art.Title));
            }
            ArticleViewModel articleViewModel = new ArticleViewModel();
            articleViewModel.Articles = articles;
            model.ArticleViewModel = articleViewModel;

            var featured = new List<KeyValuePair<int, string>>();
            foreach (var f in context.FeaturedItems.OrderBy(a => a.Title).ToList())
            {
                featured.Add(new KeyValuePair<int, string>(f.ID, f.Title));
            }
            FeaturedViewModel fViewModel = new FeaturedViewModel();
            fViewModel.FeaturedItems = featured;
            model.FeaturedViewModel = fViewModel;

            return View(model);
        }

        // get for create view
        public ActionResult CreateList()
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateList(string ListName, string PermissionReminder)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            var list = new List
            {
                Name = ListName,
                Contact = new Contact
                {
                    Company = "MDRIP",
                    Address1 = "Rondebosch",
                    City = "CapeTown",
                    State = "Western Cape",
                    Zip = "7700",
                    Country = "ZA"
                },

                PermissionReminder = PermissionReminder,
                CampaignDefaults = new CampaignDefaults
                {
                    FromEmail = "Admin@MDRIP.com",
                    FromName = "Admin",
                    Subject = "New list",
                    Language = "en-us"
                },

                EmailTypeOption = true
            };

            try
            {
                var model = await Manager.Lists.AddOrUpdateAsync(list);
                return RedirectToAction("index");
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }

            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        public ActionResult AssignRole(string Email, string Role) {
            // to write code for updating previous role to the role passed in this method
            var id = User.Identity.GetUserId();
            var roles = context.Roles.Select(r => r.Name).ToList();
            var contains = roles.IndexOf(Role);
            if (id == Email || Role == "" || contains < 0)
            {
                return RedirectToAction("Index");

            }
            foreach (var r in context.Roles.ToList())
            {
                // System.Web.Security.Roles.RemoveUserFromRole(Email, r.Name);    
                UserManager.RemoveFromRole(Email, r.Name);
            }
            UserManager.AddToRole(Email, Role);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Detail (string id)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            try
            {
                var model = await Manager.Lists.GetAsync(id);
                return View(model);
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }

            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        public async Task<ActionResult> EditList(string id)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            var model = await Manager.Lists.GetAsync(id);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditList(string id, string Name, string PermissionReminder)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            var list = new List
            {
                Id = id,
                Name = Name,
                Contact = new Contact
                {
                    Company = "MDRIP",
                    Address1 = "Rondebosch",
                    City = "CapeTown",
                    State = "Western Cape",
                    Zip = "7700",
                    Country = "ZA"
                },

                PermissionReminder = PermissionReminder,
                CampaignDefaults = new CampaignDefaults
                {
                    FromEmail = "Admin@MDRIP.com",
                    FromName = "Admin",
                    Subject = "New list",
                    Language = "en-us" 
                },

                EmailTypeOption = true 
            };

            try
            {
                var model = await Manager.Lists.AddOrUpdateAsync(list);
                return RedirectToAction("index");
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }

            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult> GetDeleteList(string id)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            var model = await Manager.Lists.GetAsync(id);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteList(string id)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            try
            {
                await Manager.Lists.DeleteAsync(id);
                return RedirectToAction("index");
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }

            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        public bool CheckPermission() {
            // returns true if the logged in user is admin, is authenticated, is activated and is confirmed
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var s = UserManager.GetRoles(user.GetUserId());
                var appUser = UserManager.FindById(User.Identity.GetUserId());
                if (!appUser.Activated || !appUser.EmailConfirmed) {
                    return false;
                } else if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public ActionResult ToggleActivation(string id, string tba)
        {
            if (!CheckPermission() || id == User.Identity.GetUserId())
            {
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
            
            try
            {
                var manager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                if (manager == null)
                {
                    var store = new UserStore<ApplicationUser>(context);
                    manager = (ApplicationUserManager)new UserManager<ApplicationUser, string>(store);
                }
                var User = manager.FindById(id);
                if (tba.ToLower() == "true")
                {
                    User.Activated = true;
                }
                else {
                    User.Activated = false;
                }
                //User.Activated = User.Activated == true ? false : true;
                manager.Update(User);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult CreateRole()
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }

            var Role = new IdentityRole();
            return View(Role);
        }

        [HttpPost]
        public ActionResult CreateRole(IdentityRole Role)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }

            context.Roles.Add(Role);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CreateAccount()
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }
            CreateAccountAdminModel model = new CreateAccountAdminModel()
            {
                Account = new AdminRegisterViewModel()
                {
                    Centers = context.Centers.ToList(),
                    Type = context.UserTypes.ToList(),
                    CountryList = context.Countries.ToList()
                },
                Roles = context.Roles.ToList()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateAccount(CreateAccountAdminModel models)
        {
            if (!CheckPermission())
            {
                return RedirectToAction("Index", "Visualisation");
            }


            var model = models.Account;
            //if (ModelState.IsValid)
            //{
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Activated = model.Activated,
                Country = model.Country,
                LabName = model.LabName,
                Specialisation = model.Specialisation,
                FieldExpertise = model.FieldExpertise,
                Center = context.Centers.Where(c => c.ID == model.UserCenter).SingleOrDefault(),
                Type = context.UserTypes.Where(c => c.ID == model.UserType).SingleOrDefault(),
            };


            var manager =UserManager;// HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var provider = new DpapiDataProtectionProvider("MDRIP");
            manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(
                provider.Create("Empty"));

            if (manager == null) {
                var store = new UserStore<ApplicationUser>(context);
                manager = (ApplicationUserManager) new UserManager<ApplicationUser, string>(store);
            }

            var result = manager.Create(user, model.Password);

            string code = manager.GenerateEmailConfirmationToken(user.Id);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            manager.SendEmail(user.Id, "Confirm your account", "An account has been created for you in MDRIP. Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    
            if (!result.Succeeded)
            {
                AddErrors(result);
            }

            result = manager.AddToRole(user.Id, model.Role);
            if (!result.Succeeded) {
                AddErrors(result);
            }
            return RedirectToAction("Index");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}