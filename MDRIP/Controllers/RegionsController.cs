﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MDRIP.Models;

namespace MDRIP.Controllers
{
	[Authorize(Roles = "Admin")]
	public class RegionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Regions
        public ActionResult Index()
        {
            return View(db.Regions.ToList());
        }

        // GET: Regions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Region region = db.Regions.Find(id);
            if (region == null)
            {
                return HttpNotFound();
            }
            return View(region);
        }

        // GET: Regions/Create
        public ActionResult Create(int? id)
        {
            var list = db.Countries.ToList();
            list.Sort((x,y)=>x.Name.CompareTo(y.Name));
            if (id != null)
            {
                var selected = db.Countries.Find(id).Name;
                if (selected != null)
                {
                    ViewBag.Selected = selected;
                }
            }
            return View(new RegionViewModel() { Countries = list });
        }

        // POST: Regions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( RegionViewModel regionViewModel)
        {
            var region = regionViewModel.Region;
            region.Country = db.Countries.Find(regionViewModel.SelectedCountry);
            // [Bind(Include = "ID,Name")]
            if (regionViewModel.SelectedCountry == null) {
                return View(regionViewModel);//new RegionViewModel() { Region = region, Countries = list });
            }
            //if (ModelState.IsValid)
            //{
                region.Country = db.Countries.Find(regionViewModel.SelectedCountry);
                db.Regions.Add(region);
                db.SaveChanges();
                return RedirectToAction("Index");
            //}

            //return View(region);
        }

        // GET: Regions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Region region = db.Regions.Find(id);
            if (region == null)
            {
                return HttpNotFound();
            }
            return View(region);
        }

        // POST: Regions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] Region region)
        {
            if (ModelState.IsValid)
            {
                db.Entry(region).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(region);
        }

        // GET: Regions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Region region = db.Regions.Find(id);
            if (region == null)
            {
                return HttpNotFound();
            }
            return View(region);
        }

        // POST: Regions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Region region = db.Regions.Find(id);
            db.Regions.Remove(region);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteRegion(int id)
        {
            var region = db.Regions.Find(id);
            db.Regions.Remove(region);
            db.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
