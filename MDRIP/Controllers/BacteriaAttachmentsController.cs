﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MDRIP.Models;

namespace MDRIP.Controllers
{
    [Authorize]
    public class BacteriaAttachmentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: BacteriaAttachments
        public ActionResult Index()
        {
            return View(db.BacteriaAttachments.ToList());
        }

        // GET: BacteriaAttachments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BacteriaAttachment bacteriaAttachment = db.BacteriaAttachments.Find(id);
            if (bacteriaAttachment == null)
            {
                return HttpNotFound();
            }
            return View(bacteriaAttachment);
        }

        // GET: BacteriaAttachments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BacteriaAttachments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Link")] BacteriaAttachment bacteriaAttachment)
        {
            if (ModelState.IsValid)
            {
                db.BacteriaAttachments.Add(bacteriaAttachment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bacteriaAttachment);
        }

        // GET: BacteriaAttachments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BacteriaAttachment bacteriaAttachment = db.BacteriaAttachments.Find(id);
            if (bacteriaAttachment == null)
            {
                return HttpNotFound();
            }
            return View(bacteriaAttachment);
        }

        // POST: BacteriaAttachments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Link")] BacteriaAttachment bacteriaAttachment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bacteriaAttachment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bacteriaAttachment);
        }

        // GET: BacteriaAttachments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BacteriaAttachment bacteriaAttachment = db.BacteriaAttachments.Find(id);
            if (bacteriaAttachment == null)
            {
                return HttpNotFound();
            }
            return View(bacteriaAttachment);
        }

        // POST: BacteriaAttachments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BacteriaAttachment bacteriaAttachment = db.BacteriaAttachments.Find(id);
            db.BacteriaAttachments.Remove(bacteriaAttachment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteAttachment(int id)
        {
            BacteriaAttachment bacteriaAttachment = db.BacteriaAttachments.Find(id);

            var path = Request.MapPath("~/" + bacteriaAttachment.Link);
            if (System.IO.File.Exists(path)) {
                System.IO.File.Delete(path);
            }
            db.BacteriaAttachments.Remove(bacteriaAttachment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
