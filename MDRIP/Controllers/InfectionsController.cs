﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MDRIP.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MDRIP.Controllers
{
    [Authorize]
    public class InfectionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        #region UserManager
        private ApplicationUserManager _userManager;

        public InfectionsController()
        {
        }
        public InfectionsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        // GET: Infections
        public ActionResult Index(string searchString)
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            var infections = from a in db.Infections select a;
            if (!String.IsNullOrEmpty(searchString))
            {
                infections = infections.Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
            }

            if (User.IsInRole("ResearcherScientist"))
            {
                return View(infections.Where(i => i.User_ID == user.Id).Include(i=>i.Attachments).Include(i => i.Bacterias).ToList());
            }
            else
            {
                return View(infections.Include(i=>i.Attachments).Include(i=>i.Bacterias).ToList());
            }
        }

        // GET: Infections/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Infection infection = db.Infections.Find(id);
            if (infection == null)
            {
                return HttpNotFound();
            }
            return View(infection);
        }

        // GET: Infections/Create
        public ActionResult Create()
        {
			List<SelectListItem> list = new List<SelectListItem>();

			foreach (Bacteria e in db.Bacteria)
			{
				SelectListItem item = new SelectListItem()
				{
					Text = e.Name,
					Value = e.ID.ToString(),
					Selected = false
				};
				list.Add(item);
			};
			InfectionViewModel model = new InfectionViewModel
			{
				BacteriaList = list,
			};
			return View(model);
        }

        // POST: Infections/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InfectionViewModel model)
        {
            if (ModelState.IsValid)
            {
				model.Infection.Bacterias = new List<Bacteria>();
				var user = UserManager.FindById(User.Identity.GetUserId());

				if (model.SelectedBacteriaList != null)
				{
					foreach (var id in model.SelectedBacteriaList)
					{
						var item = db.Bacteria.Find(Int32.Parse(id));
						if (item != null)
						{
							model.Infection.Bacterias.Add(item);
						}
					};
				}
				
				model.Infection.User_ID = User.Identity.GetUserId();
				db.Infections.Add(model.Infection);
				db.SaveChanges();

				SaveAttachments(model.Infection);

				return RedirectToAction("Index");
            }

			List<SelectListItem> list = new List<SelectListItem>();

			foreach (Bacteria e in db.Bacteria)
			{
				SelectListItem item = new SelectListItem()
				{
					Text = e.Name,
					Value = e.ID.ToString(),
					Selected = false
				};
				list.Add(item);
			};
			InfectionViewModel ivm = new InfectionViewModel
			{
				BacteriaList = list,
			};

			return View(ivm);
        }

        // GET: Infections/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

			List<SelectListItem> list = new List<SelectListItem>();

			var infection = db.Infections.Where(item => item.ID == id).Include(item => item.Attachments).Include(i => i.Bacterias).First();
			foreach (Bacteria e in db.Bacteria)
			{
				SelectListItem item = new SelectListItem()
				{
					Text = e.Name,
					Value = e.ID.ToString(),
					Selected = (infection.Bacterias.Contains(e) ? true : false)
				};
				list.Add(item);
			};

			InfectionViewModel model = new InfectionViewModel
			{
				Infection = infection,
				BacteriaList = list,
			};
			
            return View(model);
        }

        // POST: Infections/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InfectionViewModel model)
        {
            if (ModelState.IsValid)
            {
				model.Infection.Bacterias = new List<Bacteria>();
				//var user = UserManager.FindById(User.Identity.GetUserId());

				foreach (var id in model.SelectedBacteriaList)
				{
					var item = db.Bacteria.Find(Int32.Parse(id));
					if (item != null)
					{
						model.Infection.Bacterias.Add(item);
					}
				};

				model.Infection.User_ID = User.Identity.GetUserId();
				db.Infections.Add(model.Infection);
				db.SaveChanges();

				SaveAttachments(model.Infection);
				///db.Entry(bacteria).State = EntityState.Modified;
				db.SaveChanges();
				SaveAttachments(model.Infection);

				return RedirectToAction("Index");
            }
            return View(model);
        }

        // POST: Infections/Delete/5
        //[ValidateAntiForgeryToken]

        public ActionResult Delete(int id)
        {
            try
            {
                Infection infection = db.Infections.Include(i => i.Attachments).Include(i => i.Occurrences).Where(i => i.ID == id).First();
                var user = UserManager.FindById(User.Identity.GetUserId());
                var infections = from a in db.Infections select a;

                foreach (var att in infection.Attachments)
                {
                    var path = Request.MapPath("~/" + att.Link);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
                db.Infections.Remove(infection);
                db.SaveChanges();

                if (User.IsInRole("ResearcherScientist"))
                {
                    return Json(new { success = true, html = GlobalClass.RenderRazorViewToString(this, "Index", infections.Where(i => i.User_ID == user.Id).Include(i => i.Attachments).Include(i => i.Bacterias).ToList()), message = "Successfully deleted" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, html = GlobalClass.RenderRazorViewToString(this, "Index", infections.Include(i => i.Attachments).Include(i => i.Bacterias).ToList()), message = "Successfully deleted" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception)
            {
                return Json(new { success = false });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
		private void SaveAttachments(Infection infection)
		{
			var subPath = "Attachments/Infections/" + infection.ID + "/";
			bool exists = System.IO.Directory.Exists(Server.MapPath("~/" + subPath));
			if (!exists)
			{
				System.IO.Directory.CreateDirectory(Server.MapPath("~/" + subPath));
			}
			for (var i = 0; i < Request.Files.Count; i++)
			{
				HttpPostedFileBase hpf = Request.Files[i];
				var fileName = Path.GetFileName(hpf.FileName);
				if (fileName == null || fileName == "")
				{
					break;
				}

				var path = Path.Combine(subPath, fileName);
				hpf.SaveAs(Server.MapPath("~/" + path));
				InfectionAttachment a = new InfectionAttachment()
				{
					Name = fileName,
					Infection = infection,
					Link = path
				};
				db.InfectionAttachments.Add(a);
				db.SaveChanges();
			}
		}
		public ActionResult NameExists(string Name, int? Id)
		{
			var validateName = db.Infections.FirstOrDefault(x => x.Name == Name && x.ID != Id);

			if (validateName != null)
			{
				return Json(false, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(true, JsonRequestBehavior.AllowGet);
			}
		}
	}
}
