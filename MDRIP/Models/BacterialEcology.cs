﻿using System;
using System.Collections.Generic;

namespace MDRIP.Models
{
	public class BacterialEcology
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string District { get; set; }
        public string Habitat { get; set; }
        public string Prevention { get; set; }

        public ICollection<Infection> Infections { get; set; }
        public ICollection<Bacteria> Bacteria { get; set; }

		public string User_ID { get; set; }
	}
}