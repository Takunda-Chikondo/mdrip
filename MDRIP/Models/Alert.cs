﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
	public class Alert
    {
		public int ID { get; set; }
		public string Name { get; set; }
		[Display(Name = "Short Description")]
		public string ShortDescription { get; set; }
		[Display(Name = "Full Description")]
		public string FullDescription { get; set; }
		public string Importance { get; set; }
		public Infection Infection { get; set; }
		public DateTime Date { get; set; }

        public string User_ID { get; set; }
    }
}