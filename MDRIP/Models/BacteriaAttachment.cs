﻿using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
	public class BacteriaAttachment
    {
        public int ID { get; set; }
        public Bacteria Bacteria { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Link { get; set; }

        public ApplicationUser User { get; set; }

    }
}