﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDRIP.Models
{
    /// <summary>
    /// A model for the data that is sent from the visualisation tool to the APIController
    /// </summary>
    public class VisualisationData
    {
        public IEnumerable<string> Infections { get; set; }
        public IEnumerable<string> Bacteria { get; set; }
        public IEnumerable<string> Regions { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}