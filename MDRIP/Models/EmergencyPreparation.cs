﻿using System.Collections.Generic;

namespace MDRIP.Models
{
    public class EmergencyPreparation
    {
        public int ID { get; set; }
        public string CoordinatingHealthRegionalState { get; set; }
        public string DevelopingDisseminatingInformationToMedicalCommunnity { get; set; }
        public string PartnersToImplementPHDContainmentMeaseures { get; set; }
        public string CoordinteWithMCS { get; set; }
        public string EpidemiologyInvestigationActivities { get; set; }
        public string CollectingAndAnalysingDataToDevelopObjectives { get; set; }
        public ICollection<ApplicationUser> Users{ get; set; }
    }
}