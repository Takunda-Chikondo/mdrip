﻿using MvcValidationExtensions.Attribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
	public class Occurrence
    {
        public int ID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
		[Display(Name = "Reported Location")]
		public string ReportedLocation { get; set; }
		public string District { get; set; }
        [Required]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "End Date")]
		[GreaterThan("StartDate", ErrorMessage = "End date must be before the end date")]
		public DateTime EndDate { get; set; }
        public long Prevalence { get; set; }
		[Display(Name = "Death Rate")]
		public long DeathRate { get; set; }
        public Bacteria Bacteria { get; set; }
		public Infection Infection { get; set; }

		public string User_ID { get; set; }

        public ICollection<OccurrenceAttachment> Attachments { get; set; }

    }
}