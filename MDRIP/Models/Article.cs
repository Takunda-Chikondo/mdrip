﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
	public class Article
    {
		public int ID { get; set; }
        public string Title { get; set; }
		[Display(Name = "Content")]
        public string Content { get; set; }
		[Display(Name = "Publish Date")]
		public DateTime PublishDate { get; set; }
		[Display(Name = "Sequence Number")]
		public int SequenceNumber { get; set; }

        public string User_ID { get; set; }
    }
}