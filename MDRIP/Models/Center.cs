﻿using System;
using System.Collections.Generic;

namespace MDRIP.Models
{
	public class Center
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public string ReproInfection { get; set; }
        public string PatientInfection { get; set; }

		public ICollection<Bacteria> Bacteria { get; set; }
		public ICollection<Infection> Infections { get; set; }
        public ICollection<ApplicationUser> Users { get; set; }
		public ICollection<ManagingInfections> ManagingInfections { get; set; }
		public ICollection<EmergencyPreparation> EmergencyPreparations { get; set; }

	}
}