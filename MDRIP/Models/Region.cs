﻿using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
    public class Region
    {
        public int ID { get; set; }

        [Display(Name = "Region")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage ="A country is needed for the region.")]
        public Country Country { get; set; }
    }
}
