﻿using System.Collections.Generic;

namespace MDRIP.Models
{
    public class ClinicalInformation
    {
        public int ID { get; set; }
        public string ClinicName { get; set; }
        public string ClinicAddress { get; set; }
        public int ClincFormNumber { get; set; }
        public string ClinicRecords { get; set; }
        public string ClinicRecordsProviderID { get; set; }
        public string ClinicalInfectionsInformation { get; set; }
        public ICollection<Center> Centers { get; set; }
    }
}