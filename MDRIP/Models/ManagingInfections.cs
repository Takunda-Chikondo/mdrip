﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
    public class ManagingInfections
    {
        public int ID { get; set; }
		[Display(Name = "Seasonal Infection")]
		public string SeasonalInfection { get; set; }
		[Display(Name = "Spread Of Infection")]
		public string SpreadOfInfection { get; set; }
		[Display(Name = "Cause Of BaterialInfection")]
		public string CauseOfBaterialInfection  { get; set; }
		[Display(Name = "Where To Get Help")]
		public string WhereToGetHelp { get; set; }
		[Display(Name = "Bacteria Entrance")]
		public string BacteriaEntrance { get; set; }
        public string Handwashing { get; set; }
		[Display(Name = "Reducing Hospital Infection")]
		public string ReducingHospitalInfection { get; set; }
		[Display(Name = "Medicine Side Effects")]
		public string MedicinesSideEffects { get; set; }
		[Display(Name = "Safety Issues")]
		public string SafetyIssues { get; set; }
		[Display(Name = "Prescription Medicines")]
		public string PrescriptionMedicines { get; set; }
		[Display(Name = "Work Place Safetey")]
		public string WorkPlaceSafetey { get; set; }

        public ICollection<ApplicationUser> Users { get; set; }

    }
}