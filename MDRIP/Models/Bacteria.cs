﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDRIP.Models
{
    public class Bacteria
    {
        public int ID { get; set; }
		[Remote("NameExists", "Bacteria", AdditionalFields = "ID", ErrorMessage = "The bacteria already exists")]
		public string Name { get; set; }
        public string Strain { get; set; }
        public string Introduction { get; set; }
        public string Taxonomy { get; set; }
        public string Definition { get; set; }
        [Display(Name = "Aerobic/Anaerobic")]
        public string Aerobicity { get; set; }
        public string Morphology { get; set; }
        [Display(Name = "Virulence Factors")]
        public string VirulenceFactors { get; set; }
        public string Antibiotics { get; set; }
        [Display(Name = "Gram Staining")]
        public string GramStaining { get; set; }
        public string Shape { get; set; }
        [Display(Name = "Oxygen Requirement")]
        public string OxygenRequirement { get; set; }
        [Display(Name = "Clinical Characteristics")]
        public string ClinicalCharacteristics { get; set; }
        [Display(Name = "Occurrences")]
        public string Occurence { get; set; }
        [Display(Name = "Involving Diseases")]
        public string InvolingDiseases { get; set; }
        [Display(Name = "NCBI Link")]
        public string NCBI { get; set; }
        [Display(Name = "Likeliness")]
        public string Likeness { get; set; }
        public string References { get; set; }
        public string Treatment { get; set; }
        [Display(Name = "Interaction With Organism")]
        public string InteractionWithOrganism { get; set; }
        [Display(Name = "Mode Of Transmission")]
        public string ModeOfTransmission { get; set; }
        [Display(Name = "Age Group")]
        public string AgeGroup { get; set; }
        public string T = "B";
        [Display(Name = "Environmental")]
        public string Enviromental { get; set; }
		public string Adaptability { get; set; }
		public string Genetics { get; set; }
		public string District { get; set; }
		public string History { get; set; }
		public string User_ID { get; set; }

        public ICollection<Infection> Infections { get; set; }
        public ICollection<Occurrence> Occurrences { get; set; }
        public ICollection<BacteriaAttachment> Attachments { get; set; }
    }
}