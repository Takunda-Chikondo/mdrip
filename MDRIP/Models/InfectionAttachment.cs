﻿using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
    public class InfectionAttachment
    {
        public int ID { get; set; }
        public Infection Infection { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Link { get; set; }
        public ApplicationUser User { get; set; }
    }
}