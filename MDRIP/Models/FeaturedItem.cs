﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
	public class FeaturedItem
    {
		public int ID { get; set; }
        public string Title { get; set; }
		[Display(Name = "Display Message")]
        public string DisplayText { get; set; }
		[Display(Name = "Publish Date")]
		public DateTime PublishDate { get; set; }
		[Display(Name = "Sequence Number")]
		public int SequenceNumber { get; set; }
		public string Link { get; set; }
        public string ImagePath { get; set; }

        public string User_ID { get; set; }
    }
}