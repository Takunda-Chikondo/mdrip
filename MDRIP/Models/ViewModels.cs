﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDRIP.Models
{
    public class SearchResult {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Link { get; set; }

    }
    
	public class RegionViewModel {
        public Region Region { get; set; }
        public List<Country> Countries { get; set; }
        public int? SelectedCountry { get; set; }
    }
    public class CountryViewModel{
        public Country Country { get; set; }
        public List<string> Countries { get; set; }
    }

	public class OccurencesViewModel
	{
		public Occurrence Occurence { get; set; }
        public int? BacteriaID { get; set; }
        public int? InfectionID { get; set; }
        public String Type { get; set; }
        public String InfectionBacteriaID { get; set; }
        public InfectionBacteria  InfectionBacteria { get; set; }
        public IEnumerable<Bacteria> Bacteria { get; set; }
		public IEnumerable<Infection> Infection { get; set; }
        public IEnumerable<Region> Regions { get; set; }
        public IEnumerable<InfectionBacteria> InfsBacts { get; set; }
	}

    public class BacteriaViewModel
    {
        public Bacteria Bacteria { get; set; }
		public List<SelectListItem> InfectionsList { get; set; }
		public List<string> SelectedInfectionsList { get; set; }
		public IEnumerable<Infection> MyProperty { get; set; }
		public IEnumerable<ApplicationUser> Users { get; set; }
    }

	public class InfectionViewModel
	{
		public Infection Infection { get; set; }
		public List<SelectListItem> BacteriaList { get; set; }
		public List<string> SelectedBacteriaList { get; set; }
		public IEnumerable<Infection> MyProperty { get; set; }
		public IEnumerable<ApplicationUser> Users { get; set; }
	}
	public class AdminModel
    {
        public IEnumerable<Microsoft.AspNet.Identity.EntityFramework.IdentityRole> Roles { get; set; }

        public IEnumerable<ApplicationUser> Accounts { get; set; }

        public IEnumerable<MailChimp.Net.Models.List> MailingLists { get; set; }

        public AccountRoleAdminmodel AccountRole { get; set; }
        
        public ArticleViewModel ArticleViewModel { get; set; }

        public FeaturedViewModel FeaturedViewModel { get; set; }
    }

    public class ArticleViewModel {
        
        public List<KeyValuePair<int, string>> Articles;
        public Article Article { get; set; }

    }

    public class FeaturedViewModel
    {

        public List<KeyValuePair<int, string>> FeaturedItems;
        public FeaturedItem FeaturedItem { get; set; }

    }

    public class AccountRoleAdminmodel {
        public string Email { get; set; }
        public string Role { get; set; }
    }

    public class CreateAccountAdminModel {
        public AdminRegisterViewModel Account { get; set; }

        public IEnumerable<Microsoft.AspNet.Identity.EntityFramework.IdentityRole> Roles { get; set; }
    }

    public class AdminRegisterViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} is required.", MinimumLength = 2)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} is required.", MinimumLength = 2)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} is required.", MinimumLength = 2)]
        [Display(Name = "Country")]
        public string Country { get; set; }
        public IEnumerable<Country> CountryList { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 7)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public int UserCenter { get; set; }
        public int UserType { get; set; }
        public string FieldExpertise { get; set; }
        public string LabName { get; set; }
        public string Specialisation { get; set; }
        public IEnumerable<Center> Centers { get; set; }
        public IEnumerable<UserType> Type { get; set; }
        [Required]
        [Display(Name = "Role of the User")]
        public string Role { get; set; }
        public bool Activated { get; set; }
    }

    public class LandingViewModel
    {
        public List<Alert> Alerts{ get; set; }
        public List<Article> Articles { get; set; }
        public List<FeaturedItem> FeaturedItems { get; set; }

    }
}
