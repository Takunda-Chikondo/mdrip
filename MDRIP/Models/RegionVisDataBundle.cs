﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MDRIP.Models
{
    /// <summary>
    /// Defines a bundle for a region with its infections and bacteria
    /// </summary>
    public class RegionDataBundle
    {
        public string RegionName { get; set; }
        public int RegionID { get; set; }
        public IEnumerable<InfBactData> Results { get; set; }
    }


    /// <summary>
    /// This contains information for a bacteria or an infection that is returned to the frontend
    /// </summary>
    public class InfBactData
    {
        public bool IsBacteria { get; set; }
        public int? ID { get; set; }
        public bool IsNull = true;
        public string Name { get; set; }
        public string InfType { get; set; }
        public string Definition { get; set; }
        public string InfClassification { get; set; }
        public string InfSymptoms { get; set; }
        public string InfSeasonal { get; set; }
        public string InfPathophysiology { get; set; }
        public string InfDiagnosis { get; set; }
        public string InfPrevention { get; set; }
        public string InfEpidemiology { get; set; }
        public string InfReferences { get; set; }
        public string InfHistory { get; set; }
        public List<KeyValue> InfBacteriaList { get; set; }
        public List<KeyValue> InfClinicalInformationList { get; set; }

        public string BactIntroduction { get; set; }
        public string BactStrain { get; set; }
        public string BactTaxonomy { get; set; }
        public string BactGramStaining { get; set; }
        public string BactShape { get; set; }
        public string BactOxygenRequirement { get; set; }
        public string BactClinicalCharacteristics { get; set; }
        public string BactOccurrence { get; set; }
        public string BactInvolvingDiseases { get; set; }
        public string BactNCBI { get; set; }
        public string BactLikeness { get; set; }
        public string BactInteractions { get; set; }
        public List<KeyValue> BactInfections { get; set; }
        public string BactAerobicity { get; set; }
        public string BactMorphology { get; set; }
        public string BactVirulenceFactors { get; set; }

        //public IEnumerable<KeyValue> Prevalence { get; set; }
        //public IEnumerable<KeyValue> DeathRate { get; set; }
        //public double Latitude { get; set; }
        //public double Longitude { get; set; }
        //public string LocationName { get; set; }
        public List<Occur> Occurrences { get; set; }
        public List<Occur> OccurrencesOriginal { get; set; }
        public string Treatment { get; set; }
        public string References { get; set; }
        public List<KeyValue> Attachments { get; set; }
        public string ModeofTransmissions { get; set; }
        public string Antibiotic { get; set; }
        public long TotalPrevalences { get; set; }
        public long TotalDeathRates { get; set; }

        public string AgeGroup { get; set; }
        public string District { get; set; }
        public string Environmental { get; set; }
        public string Adaptability { get; set; }
        public string Genetics { get; set; }
    }

    public class KeyValue
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Represents the occurrence of an infection/bacteria
    /// </summary>
    public class Occur {
        public DateTime StartDate { get; set; }
        public string EndDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Location { get; set; }
        public long DeathRate { get; set; }
        public long Prevalence { get; set; }
    }
}