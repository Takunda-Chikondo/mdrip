﻿using System.ComponentModel.DataAnnotations;

namespace MDRIP.Models
{
	public class OccurrenceAttachment
    {
        public int ID { get; set; }
        public Occurrence Occurrence { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Link { get; set; }

        public ApplicationUser User { get; set; }

    }
}