﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MDRIP.Models
{
    public class Infection
    {
        public int ID { get; set; }
        public string T = "I";
        public string Type { get; set; }
		[Remote("NameExists", "Infections", AdditionalFields = "ID", ErrorMessage = "The infection already exists")]
		public string Name { get; set; }
        public string Definition { get; set; }
        public string Classification { get; set; }
        public string Symptoms { get; set; }
        public string Seasonal { get; set; }
        public string Pathophysiology { get; set; }
        [Display(Name = "Mode Of Transmission")]
        public string ModeOfTransmission { get; set; }
        public string Diagnosis { get; set; }
        public string Prevention { get; set; }
        [Display(Name = "Epidemiology")]
        public string Epidimology { get; set; }
        public string References { get; set; }
        public string History { get; set; }
        public string Treatment { get; set; }
        public string Antibiotics { get; set; }

        [Display(Name = "Age Group")]
        public string AgeGroup { get; set; }
        [Display(Name = "Environmental")]
        public string Enviromental { get; set; }
		public string Adaptability { get; set; }
		public string Genetics { get; set; }
		public string District { get; set; }

		public ICollection<ClinicalInformation> ClinicalInformation { get; set; }
        public ICollection<Bacteria> Bacterias { get; set; }
        public ICollection<Occurrence> Occurrences { get; set; }
        public string User_ID { get; set; }
        public ICollection<InfectionAttachment> Attachments { get; set; }
    }
}