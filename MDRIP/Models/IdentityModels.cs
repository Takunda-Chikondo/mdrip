﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MDRIP.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("FirstName", this.FirstName));
            
            return userIdentity;
        }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Country { get; set; }
        public bool Activated { get; set; }
        public UserType Type { get; set; }
        public Center Center { get; set; }
        public string FieldExpertise { get; set; }
        public string LabName { get; set; }
        public string Specialisation { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<MDRIP.Models.Alert> Alerts { get; set; }
        public System.Data.Entity.DbSet<MDRIP.Models.EmergencyPreparation> EmergencyPreparations { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Bacteria> Bacteria { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.BacterialEcology> BacterialEcologies { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.ClinicalInformation> ClinicalInformations { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Infection> Infections { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.ManagingInfections> ManagingInfections { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Country> Countries { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Region> Regions { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Antibiotic> Antibiotics { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Center> Centers { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Occurrence> Occurrences { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.UserType> UserTypes { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.BacteriaAttachment> BacteriaAttachments { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.InfectionAttachment> InfectionAttachments { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.Article> Articles { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.FeaturedItem> FeaturedItems { get; set; }

        public System.Data.Entity.DbSet<MDRIP.Models.OccurrenceAttachment> OccurrenceAttachments { get; set; }
    }
}