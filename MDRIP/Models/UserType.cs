﻿using System.Collections.Generic;

namespace MDRIP.Models
{
	public class UserType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<ApplicationUser> Users { get; set; } 
    }
}