function refreshVis(result) {
    var showDeathRate = document.getElementById('parameterDeathRate').classList.contains("vis-badge-selected") ? true : false;
    var showPrevalence =  document.getElementById('parameterPrevalence').classList.contains("vis-badge-selected") ? true : false;
    var both = document.getElementById('parameterBoth').classList.contains("vis-badge-selected") ? true : false;
    
    var element = $('#chartSelection');
    var selected = element.val();
    switch (selected) {
        case "Pie Chart":
        case "Bar Chart":
            barchart(result, showPrevalence, showDeathRate, both);
            break;
        case "Scatter Plot":
            scatter(showPrevalence, showDeathRate, both);
            break;
        case "Line Graph":
            lineGraph(showPrevalence, showDeathRate, both);
            break;
        case "Choropleth Map":
            choropleth(showPrevalence, showDeathRate, both);
            break;
        case "Geographic Map":
            geographicmap();
            break;
        default:
    }
}

function barchart(result, showPrevalence, showDeathRate, showBoth) {

    resetVisualisation();
    var visDiv = $('#visualisation');

    var barchart = document.createElement('div');
    barchart.id = 'barchart';
    barchart.setAttribute("style", "width:50%; height;100%; min-height:400px;text-align: center; float:left; ");
    visDiv[0].appendChild(barchart);

    var div = document.createElement('div');
    div.setAttribute("style", "width:50%; height;100%; float:right;");
    
    visDiv[0].appendChild(div);

    var piechart = document.createElement('div');
    piechart.id = 'piechartOne';
    piechart.setAttribute("style", "width:100%; height;100%; float:right; clear:both");
    div.appendChild(piechart);//visDiv[0].appendChild(piechart);

    var piechartTwo = document.createElement('div');
    piechartTwo.id = 'piechartTwo';
    piechartTwo.setAttribute("style", "width:100%; height;100%; float:right; clear:both");
    div.appendChild(piechartTwo);

    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });
    console.log(selectedInfections);
    console.log(selectedRegions);
    console.log(result);
    
    var forBarchart = [];
    var forPiechart = [];
    var infectionsPrevalence = {};
    var infectionsDeathrate = {};
    var i = 0;

    for (var item in result) {
        var value = result[item];
        var push = {};
        if (selectedRegions.indexOf(value['RegionName']) >= 0) {
            push.y = value['RegionName'];

            var totalprevalences = 0;
            var totaldeaths = 0;
            for (var subitem in value['Results']) {
                if (selectedInfections.indexOf(value["Results"][subitem]["InfectionName"]) >= 0) {
                    prevalences = value['Results'][subitem]['Prevalence'];
                    for (var subsubitem in prevalences) {
                        //if (prevalences[subsubitem]['Date']) {
                            totalprevalences += prevalences[subsubitem]["Value"];
                        //}
                    }
                    if (i == 0) {
                        infectionsPrevalence[value["Results"][subitem]["InfectionName"]] = totalprevalences;
                    } else {
                        infectionsPrevalence[value["Results"][subitem]["InfectionName"]] += totalprevalences;
                    }

                    deathRates = value['Results'][subitem]['DeathRate'];
                    for (var subsubitem in deathRates) {
                        //if (deathRates[subsubitem]['Date']) {
                            totaldeaths += deathRates[subsubitem]["Value"];
                        //}
                    }
                    if (i == 0) {
                        infectionsDeathrate[value["Results"][subitem]["InfectionName"]] = totaldeaths;
                    } else {
                        infectionsDeathrate[value["Results"][subitem]["InfectionName"]] += totaldeaths;
                    }

                }
            }
            push.a = totalprevalences;
            push.b = totaldeaths;
            forBarchart.push(push);
        }
        i++;
    }


    ykeys = []; labels = [];
    if ((showPrevalence && showDeathRate) || showBoth){
        ykeys=['a', 'b']; labels=['Prevalence', 'Death Rate'];
    } else {
        if (showPrevalence){
            ykeys = ['a']; labels = ['Prevalence'];
        } else {
            ykeys=['b']; labels=['Death Rate'];
        }
    }
    Morris.Bar({
        element: 'barchart',
        data: forBarchart,
        xkey: 'y',
        ykeys: ykeys,
        hideHover: 'auto',
        labels: labels
    });
    var pieData = [];
    var pieDataTwo = [];
    if ((showPrevalence && showDeathRate) || showBoth) {
        //for (var i in forBarchart) {
        //    pieData.push({ label: "Prevalence in "+forBarchart[i]['y'], value: forBarchart[i]['a'] });
        //    pieDataTwo.push({ label: "Death Rate in "+forBarchart[i]['y'], value: forBarchart[i]['b'] });
        //}
        for (var i in infectionsPrevalence) {
            pieData.push({ label: "Prevalence of " + i, value: infectionsPrevalence[i] });
        }
        for (var i in infectionsDeathrate) {
            pieDataTwo.push({ label: "Death Rate of "+i, value: infectionsDeathrate[i] });
        }
        Morris.Donut({
            element: 'piechartOne',
            data: pieData
        });
        Morris.Donut({
            element: 'piechartTwo',
            data: pieDataTwo
        });
    } else {
        if (showPrevalence) {
            //for (var i in forBarchart) {
            //    pieData.push({ label:"Prevalence in " + forBarchart[i]['y'], value: forBarchart[i]['a']});
            //}
            for (var i in infectionsPrevalence) {
                pieData.push({ label: "Prevalence of " + i, value: infectionsPrevalence[i] });
            }
            Morris.Donut({
                element: 'piechartOne',
                data: pieData
            });
        } else {
            //for (var i in forBarchart) {
            //    pieData.push({ label: "Death Rate in " + forBarchart[i]['y'], value: forBarchart[i]['b'] });
            //}
            for (var i in infectionsDeathrate) {
                pieDataTwo.push({ label: "Death Rate of " + i, value: infectionsDeathrate[i] });
            }
            Morris.Donut({
                element: 'piechartOne',
                data: pieData
            });
        }
    }
}

function toggleInfectionsAndRegions(className) {
    var items = document.getElementsByClassName(className);

    if (items === null) {
        items = $('.' + className);
    }
    for (var i = 0; i < items.length; i++) {
        items[i].addEventListener('click', function(event) {
            $(this).toggleClass("vis-badge-selected");
            $(this).toggleClass("vis-badge-notselected");
            refreshVis(window.ReceivedData);
        });
    }
}

$('#chartSelection').change(function() {
    //var element = $('#chartSelection');
    //var selected = element.val();
    resetVisualisation();
    refreshVis(window.ReceivedData);
    //switch (selected) {
    //    case "Pie Chart":
    //        barchart();//donut();
    //        break;
    //    case "Bar Chart":
    //        barchart();
    //        break;
    //    case "Scatter Plot":
    //        scatter();
    //        break;
    //    case "Line Graph":
    //        lineGraph();
    //        break;
    //    case "Choropleth Map":
    //        choropleth();
    //        break;
    //    case "Geographic Map":
    //        geographicmap();
    //        break;
    //    default:
    //}
});

function resetVisualisation() {
    $('#visualisation').innerHTML = "";
    $("#visualisation").html("");
}

function geographicmap() {
    var mapProp = {
        center: new google.maps.LatLng(30.3, 69.3),
        zoom: 5,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            }
        ]
    };

    var map = new google.maps.Map(document.getElementById("visualisation"), mapProp);
    
    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });
    
    var result = window.ReceivedData;
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 400
    });
    for (var item in result) {
        var value = result[item];
        if (selectedRegions.indexOf(value['RegionName']) >= 0) {
            var totalprevalences = 0;
            var totaldeaths = 0;
            for (var subitem in value['Results']) {
                if (selectedInfections.indexOf(value["Results"][subitem]["InfectionName"]) >= 0) {
                    prevalences = value['Results'][subitem]['Prevalence'];
                    for (var subsubitem in prevalences) {
                        totalprevalences += prevalences[subsubitem]["Value"];
                    }

                    deathRates = value['Results'][subitem]['DeathRate'];
                    for (var subsubitem in deathRates) {
                        totaldeaths += deathRates[subsubitem]["Value"];
                    }

                    var it = value['Results'][subitem];

                    modalDetails = '<div id="content" style="color:black">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<h5 id="firstHeading" class="firstHeading">' + it['InfectionName'] + '</h5><h6>' + it['Introduction'] + '</h6>' +
                        '<div id="bodyContent">' +
                        '<p><b>Prevalence: ' + totalprevalences + '</b>,<br/>' +
                        '<b>Death Rate: ' + totaldeaths + '</b>,<br/>' +
                        '<b>Bateria: <i>' + it['Bacteria'] + '</i> (' + it['Aerobicity'] + ')</b>,<br/>' +
                        '<b>Infection: ' + it['InfectionName'] +
                        '<p><table style="width:100%"> <tr> <td>' +
                        'Timeline: ' + it['TimelineStart'] + ' - ' + it['TimelineEnd'] + '<br /> </td> <td>' +
                        'Affected People: <Gender> and <Age><br /> </td> </tr> <tr> <td> Strain: ' + it['Strain'] + ' <br />' +
                        '    </td> <td> Taxonomy: ' + it['Taxonomy'] + '<br /> </td> </tr> <tr> <td> Morphology: ' + it['Morphology'] +
                        '<br />  </td> <td> Virulence Factors: ' + it['VirulenceFactors'] + '<br />' +
                        '    </td> </tr> <tr> <td> Interaction with Organisms: ' + it['Interactions'] + ' <br /> </td> <td> Antibiotics: ' + it['Antibiotics'] + ' <br /> </td> </tr>' +
                        '<tr> <td> Treatments: ' + it['Treatment'] + ' <br /> </td>' +
                        '</tr></table></p>' +
                        '<p>Location: ' + it['LocationName'] + ' (<a href="#">Lat: ' + it['Latitude'] + ', Long: ' + it['Longitude'] + '</a>)</p>' +
                        '<a href="#">See References</p>' +
                        '</div>' +
                        '</div>';

                    var contentString = '<div id="content" style="color:black">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<h5 id="firstHeading" class="firstHeading">' + it['InfectionName'] +'</h5>' +
                        '<div id="bodyContent">' +
                        '<p><b>Prevalence: ' + totalprevalences + '</b>,<br/>' +
                        '<b>Death Rate: ' + totaldeaths + '</b>,<br/>' +
                        '<b>Bateria: <i>' + it['Bacteria'] + '</i> (' + it['Aerobicity'] +')</b>,<br/>' +
                        '<p>Location: ' + it['LocationName'] + ' (<a href="#">Lat: ' + it['Latitude'] + ', Long: ' + it['Longitude'] +'</a>)</p>' +
                        '<button type="button"  id="launchModal" class="btn btn-primary" onclick="launchModal()" data-toggle="modal" data-target="#exampleModalLong"> See All Details </button >' +
                        '</div>' +
                        '</div>';

                   

                    var lat = parseFloat(it['Latitude']);
                    var long = parseFloat(it['Longitude']);

                    var marker = new google.maps.Marker({
                        position: { lat: lat, lng: long },
                        map: map,
                        icon: getCircle(4)
                    });
                    
                    google.maps.event.addListener(marker, 'click', (function (marker, content, details) {
                        return function () {
                            infowindow.setContent(content);
                            infowindow.open(map, marker);
                            window.ModalDetails = details;
                        }
                    })(marker, contentString, modalDetails));
                }
            }
        }
    }    
}

function launchModal() {
    document.getElementById('modal-body').innerHTML = window.ModalDetails;
}

function getCircle(magnitude) {
    return {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: 'red',
        fillOpacity: .2,
        scale: Math.pow(2, magnitude) / 2,
        strokeColor: 'white',
        strokeWeight: .5
    };
}

function choropleth(showPrevalence, showDeathRate, showBoth) {
    var mapProp = {
        center: new google.maps.LatLng(30.3, 69.3),
        zoom: 5,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            }
        ]
    };
    var map = new google.maps.Map(document.getElementById("visualisation"), mapProp);

    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });


    var results = [];
    var result = window.ReceivedData;
    var maxDt = 0; var maxPrev = 0;

    for (var item in result) {
        var value = result[item];
        var push = {};
        //if (selectedRegions.indexOf(value['RegionName']) >= 0) {
            push.region = value['RegionName'];

            var totalprevalences = 0;
            var totaldeaths = 0;
            for (var subitem in value['Results']) {
                if (selectedInfections.indexOf(value["Results"][subitem]["InfectionName"]) >= 0) {
                    prevalences = value['Results'][subitem]['Prevalence'];
                    for (var subsubitem in prevalences) {
                        totalprevalences += prevalences[subsubitem]["Value"];
                    }

                    deathRates = value['Results'][subitem]['DeathRate'];
                    for (var subsubitem in deathRates) {
                        totaldeaths += deathRates[subsubitem]["Value"];
                    }
                }
            }
            push.prev = totalprevalences;
            push.dt = totaldeaths;
            results.push(push);
            maxPrev = totalprevalences > maxPrev ? totalprevalences : maxPrev;
            maxDt = totaldeaths > maxDt ? totaldeaths : maxDt; 
        //}
    }

    var infowindow = new google.maps.InfoWindow();
    map.data.addListener('mouseover', function (event) {
        var str = "<div style='width:auto; color: #0f0f0f'>" +
            "<table>" +
            "<tr> <td>Location: </td><td>" + event.feature.getProperty('districts') + ", " + event.feature.getProperty('province_territory') + "</td>" +
            "<tr><td>Prevalence: </td><td>N/A</td></tr>" +
            "<tr><td>Death Rate: </td><td>N/A</td></tr>" +
                    "</table></div>";
        for (var i in results) {
            var item = results[i];
            if (item.region == event.feature.getProperty('districts')) {
                str = "<div style='width:auto; color: #0f0f0f'>" +
                    "<table>" +
                    "<tr> <td>Location: </td><td>" + event.feature.getProperty('districts') + ", " + event.feature.getProperty('province_territory') + "</td>" +
                    "<tr><td>Prevalence: </td><td>"+item.prev+"</td></tr>" +
                    "<tr><td>Death Rate: </td><td>" + item.dt +"</td></tr>" +
                    "</table></div>";
                break;
            }
        }
        infowindow.setContent(str);
        map.data.revertStyle();
        map.data.overrideStyle(event.feature, { strokeWeight: 2 });

        
        //var coordOne = event.feature.getGeometry().getAt(0).getAt(0).getAt(0);
        //var newLat = (coordOne.lat() + event.latLng.lat()) / 2;
        //var newLng = (coordOne.lng() + event.latLng.lng()) / 2;
        //var newCoord = new google.maps.LatLng(newLat, newLng);

        // var heatmapData = [event.feature.getGeometry().getAt(0).getAt(0).getAt(0)];
        // var heatmap = new google.maps.visualization.HeatmapLayer({
        //     data: heatmapData
        // });
        // heatmap.setMap(map);

        infowindow.setPosition(event.latLng);
        infowindow.setOptions({ pixelOffset: new google.maps.Size(0,0) });
        infowindow.open(map);
    });
    map.data.loadGeoJson('/Assets/map/pakistan.json');//'@Url.Content("~/Assets/map/pakistan.json")');

    map.data.setStyle(function (feature) {
        var color = "#f7fbff";
        for (var i in results) {
            var item = results[i];
            if (item.region == feature.getProperty('districts')) {
                if (showDeathRate || (showDeathRate && showPrevalence && !showPrevalence)) {
                    color = getChoroplethColour(item.dt, maxDt)
                } else {
                    color = getChoroplethColour(item.prev, maxPrev);//item.prevcolour;
                }
                break;
            }
        }
        return {
            fillColor: color,
            strokeWeight: 0.5
        };
    });

    

}

function getChoroplethColour(value, max) {
    var Colours = ['#fff7ec', '#fee8c8', '#fdd49e', '#fdbb84', '#fc8d59', '#ef6548', '#d7301f', '#b30000', '#7f0000'];

        // ['#f7fbff', '#deebf7', '#c6dbef', '#9ecae1', '#6baed6', '#4292c6', '#2171b5', '#08519c', '#08306b'];

    var val = (value / max) * (Colours.length - 1);
    val = Math.round(val);
    val = val % Colours.length;
    console.log(Colours[val]);
    return Colours[val];
}

function lineGraph(showPrevalence, showDeathRate, showBoth) {

    resetVisualisation();

    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });
    
    var infectionsPrevalence = {};
    var infectionsDeathrate = {};
    var result = window.ReceivedData
    for (var item in result) {
        var value = result[item];
        if (selectedRegions.indexOf(value['RegionName']) >= 0) {
            var i = 0;
            for (var subitem in value['Results']) {
                if (selectedInfections.indexOf(value["Results"][subitem]["InfectionName"]) >= 0) {
                    prevalences = value['Results'][subitem]['Prevalence'];
                    for (var subsubitem in prevalences) {
                        if (i == 0) {
                            infectionsPrevalence[prevalences[subsubitem]['Date']] = prevalences[subsubitem]['Value'];
                        } else {
                            infectionsPrevalence[prevalences[subsubitem]['Date']] += prevalences[subsubitem]['Value'];
                        }
                    }
                    

                    deathRates = value['Results'][subitem]['DeathRate'];
                    for (var subsubitem in deathRates) {
                        if (i == 0) {
                            infectionsDeathrate[deathRates[subsubitem]['Date']] = deathRates[subsubitem]['Value'];
                        } else {
                            infectionsDeathrate[deathRates[subsubitem]['Date']] += deathRates[subsubitem]['Value'];
                        }
                    }
                    i++;
                }
            }
        }
    }
    var data = [];
    var labels = []; ykeys = [];
    if ((showDeathRate && showPrevalence) || showBoth) {
        for (var i in infectionsPrevalence) {
            var push = {};
            push.x = i;
            push.y = infectionsPrevalence[i];
            push.z = infectionsDeathrate[i];
            data.push(push);
        }
        labels = ["Prevalence", "Death Rate"];
        ykeys = ['y', 'z'];
    } else if (showDeathRate) {
        for (var i in infectionsDeathrate) {
            var push = {};
            push.x = i;
            push.y = infectionsDeathrate[i];
            data.push(push);
        }
        labels = ["Death Rate"];
        ykeys = ['y'];
    } else {// show prevalence
        for (var i in infectionsPrevalence) {
            var push = {};
            push.x = i;
            push.y = infectionsPrevalence[i];
            data.push(push);
        }
        ykeys = ['y'];
        labels = ["Prevalence"];
    }

    $('#visualisation').css("padding", "50px");

    Morris.Line({
        element: 'visualisation',
        data: data,
        xkey: 'x',
        ykeys: ykeys,
        smooth: false,
        hideHover: 'auto',
        axes: true,
        grid: true,
        labels: labels
    });
}

function scatter(showPrevalence, showDeathRate, both) {

    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });


    // need to check if both prevalence and deathrate is selected. assuming yes here;
    var regions = [];
    var results = [];
    var result = window.ReceivedData;
    for (var item in result) {
        var value = result[item];
        if (selectedRegions.indexOf(value['RegionName']) >= 0) {
            regions.push(value['RegionName']);


            var deathrates = {};
            var prevs = {};
            for (var subitem in value['Results']) {
                var totalprevalences = 0;
                var totaldeaths = 0;
                if (selectedInfections.indexOf(value["Results"][subitem]["InfectionName"]) >= 0) {
                    prevalences = value['Results'][subitem]['Prevalence'];
                    for (var subsubitem in prevalences) {
                        totalprevalences += prevalences[subsubitem]["Value"];
                    }
                    prevs[value["Results"][subitem]["InfectionName"]] = totalprevalences;

                    deathRates = value['Results'][subitem]['DeathRate'];
                    for (var subsubitem in deathRates) {
                        totaldeaths += deathRates[subsubitem]["Value"];
                    }
                    deathrates[value["Results"][subitem]["InfectionName"]] = totaldeaths;
                }
            }
            results[value['RegionName']] = [prevs, deathrates];
        }
    }


    var trsPrevs = {}; var trsDts = {};
    for (var reg in regions) {
        var item = results[regions[reg]];
        var prevs = item[0];
        var dts = item[1];
        for (var p in prevs) {
            if (trsPrevs[p] == null) {
                trsPrevs[p] = [];
            }
            (trsPrevs[p]).push(prevs[p] == null ? 0 : prevs[p]);
        }
        for (var d in dts) {
            //p is infection name and prevs[p] is the prevalence
            if (trsDts[d] == null) {
                trsDts[d] = [];
            }
            (trsDts[d]).push(dts[d] == null ? 0 : dts[d]);
        }
    }
    
    if (showDeathRate || (showDeathRate && !showPrevalence)) {
        var traces = [];
        for (var tr in trsDts) {
            var trace = {};
            trace.x = regions;
            trace.y = trsDts[tr];
            trace.mode = "markers";
            trace.type = "scatter";
            trace.name = tr;
            trace.marker = { size: 12 };
            traces.push(trace);
        }

        var layout = {
            xaxis: {
                range: [-1, regions.length + 1], title: "Regions", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            yaxis: {
                range: [0, 100], title: "Prevalence", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            title: 'Prevalence of Selected Infections/Bacteria at Selected Regions'
        };
    } else /* if (showPrevalence || (showPrevalence && showBoth && !showDeathRate))*/ {
        var traces = [];
        for (var tr in trsPrevs) {
            var trace = {};
            trace.x = regions;
            trace.y = trsPrevs[tr];
            trace.mode = "markers";
            trace.type = "scatter";
            trace.name = tr;
            trace.marker = { size: 12 };
            traces.push(trace);
        }

        var layout = {
            xaxis: {
                range: [-1, regions.length + 1], title: "Regions", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            yaxis: {
                range: [0, 100], title: "Prevalence", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            title: 'Prevalence of Selected Infections/Bacteria at Selected Regions'
        };
    }


    


    //var regions = ['Region 1', 'Region 2', 'Region 3'];
    //var trace1 = {
    //    x: regions,
    //    y: [1, 2, 3],
    //    mode: 'markers',
    //    type: 'scatter',
    //    name: 'Infection 1',
    //    marker: { size: 12 }
    //};

    //var trace2 = {
    //    x: regions,
    //    y: [2, 4, 6],
    //    mode: 'markers',
    //    type: 'scatter',
    //    name: 'Infection 2',
    //    marker: { size: 12 }
    //};

    //var trace3 = {
    //    x: regions,
    //    y: [3, 6, 9],
    //    mode: 'markers',
    //    type: 'scatter',
    //    name: 'Infection 3',
    //    marker: { size: 12 }
    //};

    // var data = [trace1, trace2, trace3];

   

    Plotly.newPlot('visualisation', traces, layout);
}