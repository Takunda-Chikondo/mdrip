/**
 * This file has various functions that the visualisation tool uses.
 * Almost all functions correspond to one visualisation.
 * Different JS library accepts data for visualisation in different format.
 * The results returned by the server is stored in the global variable ReceivedData, accessible by calling window.ReceivedData
 * Structure of ReceivedData is defined in the APIController->GetVisualisationData
 * Basically, the receivedData is converted into the form required by the library and then passed on to the
 * library for it to render the visualisation.
 * @param {any} result
 */


// This function refreshes the appropriate visualisation when it is called
function refreshVis(result) {
    var showDeathRate = document.getElementById('parameterDeathRate').classList.contains("vis-badge-selected") ? true : false;
    var showPrevalence =  document.getElementById('parameterPrevalence').classList.contains("vis-badge-selected") ? true : false;
    var both = document.getElementById('parameterBoth').classList.contains("vis-badge-selected") ? true : false;

    var element = $('#chartSelection');
    var selected = element.val();
    switch (selected) {
        case "Pie Chart":
        case "Bar/Pie Chart":
            $('.scale').css("display", "none");
            barchart(result, showPrevalence, showDeathRate, both);
            break;
        case "Scatter Plot":
            $('.scale').css("display", "none");
            scatter(showPrevalence, showDeathRate, both);
            break;
        case "Line Graph":
            $('.scale').css("display", "none");
            lineGraph(showPrevalence, showDeathRate, both);
            break;
        case "Choropleth Map":
            $('.scale').css("display", "block");
            choropleth(showPrevalence, showDeathRate, both);
            break;
        case "Geographic Map":
            $('.scale').css("display", "none");
            geographicmap(showPrevalence, showDeathRate, both);
            break;
        default:
    }
}

// Function to display the barchart 
function barchart(result, showPrevalence, showDeathRate, showBoth) {
    displayNotification("Success", "Bar chart/Pie chart visualisation chosen.");
    resetVisualisation();
    var visDiv = $('#visualisation');

    var barchart = document.createElement('div');
    barchart.id = 'barchart';
    barchart.setAttribute("style", "width:50%; height;100%; min-height:400px;text-align: center; float:left; ");
    visDiv[0].appendChild(barchart);

    var div = document.createElement('div');
    div.setAttribute("style", "width:50%; height;100%; float:right;");
    
    visDiv[0].appendChild(div);

    var piechart = document.createElement('div');
    piechart.id = 'piechartOne';
    piechart.setAttribute("style", "width:100%; height;100%; float:right; clear:both");
    div.appendChild(piechart);

    var piechartTwo = document.createElement('div');
    piechartTwo.id = 'piechartTwo';
    piechartTwo.setAttribute("style", "width:100%; height;100%; float:right; clear:both");
    div.appendChild(piechartTwo);

    var selectedRegions = [];
    var selectedRegIds = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
            selectedRegIds.push(button.getAttribute("data-index"));
        }
    });

    var selectedInfections = [];
    var selectedInfIds = []; 
    var selectedBactIds = []; 
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            if (button.getAttribute("data-type") === "I") {
                selectedInfIds.push(button.getAttribute("data-index"));
            } else {
                selectedBactIds.push(button.getAttribute("data-index"));
            }
        }
    });
    
    var forBarchart = [];
    var infectionsPrevalence = {};
    var infectionsDeathrate = {};
    var initialised = {};

    for (var item in result) {
        var value = result[item];
        var push = {};
        if (selectedRegIds.indexOf(value["RegionID"]+"")>=0) {
            push.y = value['RegionName'];

            var totalprevalences = 0;
            var totaldeaths = 0;
            for (var subitem in value['Results']) {
                if ((value['Results'][subitem]["IsBacteria"] && selectedBactIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0) || (!value['Results'][subitem]["IsBacteria"] &&selectedInfIds.indexOf((value['Results'][subitem]["ID"] )+"") >= 0)) {//if (selectedInfections.indexOf(value["Results"][subitem]["Name"]) >= 0) {
                    prevalences = value['Results'][subitem]['TotalPrevalences'];
                    deathRates = value['Results'][subitem]['TotalDeathRates'];
                    if (initialised[value["Results"][subitem]["Name"]] == null) {
                        infectionsPrevalence[value["Results"][subitem]["Name"]] = prevalences;
                        infectionsDeathrate[value["Results"][subitem]["Name"]] = deathRates;
                        initialised[value["Results"][subitem]["Name"]] = "Initialised";
                    } else {
                        infectionsPrevalence[value["Results"][subitem]["Name"]] += prevalences;
                        infectionsDeathrate[value["Results"][subitem]["Name"]] += deathRates;
                    }

                    totalprevalences += prevalences;
                    totaldeaths += deathRates;
                }
            }
            push.a = totalprevalences;
            push.b = totaldeaths;
            forBarchart.push(push);
        }
    }

    ykeys = []; labels = [];
    if ((showPrevalence && showDeathRate) || showBoth) {
        displayNotification("Success", "Showing: Prevalence and Death Rates");
        ykeys=['a', 'b']; labels=['Prevalence', 'Death Rate'];
    } else {
        if (showPrevalence) {
            displayNotification("Success", "Showing: Prevalence");
            ykeys = ['a']; labels = ['Prevalence'];
        } else {
            displayNotification("Success", "Showing: Death Rate");
            ykeys=['b']; labels=['Death Rate'];
        }
    }
    Morris.Bar({
        element: 'barchart',
        data: forBarchart,
        xkey: 'y',
        ykeys: ykeys,
        hideHover: 'auto',
        resize:true,
        labels: labels
    });

    var pieData = [];
    var pieDataTwo = [];
    if ((showPrevalence && showDeathRate) || showBoth) {
        for (var i in infectionsPrevalence) {
            pieData.push({ label: "Prevalence of " + i, value: infectionsPrevalence[i] });
        }
        for (var i in infectionsDeathrate) {
            pieDataTwo.push({ label: "Death Rate of "+i, value: infectionsDeathrate[i] });
        }
        Morris.Donut({
            element: 'piechartOne',
            data: pieData
        });
        Morris.Donut({
            element: 'piechartTwo',
            data: pieDataTwo
        });
    } else {
        if (showPrevalence) {
            for (var i in infectionsPrevalence) {
                pieData.push({ label: "Prevalence of " + i, value: infectionsPrevalence[i] });
            }
            Morris.Donut({
                element: 'piechartOne',
                data: pieData
            });
        } else {
            for (var i in infectionsDeathrate) {
                pieData.push({ label: "Death Rate of " + i, value: infectionsDeathrate[i] });
            }
            Morris.Donut({
                element: 'piechartOne',
                data: pieData
            });
        }
    }
}

// This makes the paramaters on the parameters bar clickable. It takes in a class name so that 
// it does not need to go through the list of all the 
function toggleInfectionsAndRegions(className) {
    var items = document.getElementsByClassName(className);

    if (items === null) {
        items = $('.' + className);
    }
    for (var i = 0; i < items.length; i++) {
        items[i].addEventListener('click', function(event) {
            $(this).toggleClass("vis-badge-selected");
            $(this).toggleClass("vis-badge-notselected");
            refreshVis(window.ReceivedData);
        });
    }
}

// Call the refreshvisualisation method when the dropdown is changed
$('#chartSelection').change(function() {
    resetVisualisation();
    refreshVis(window.ReceivedData);
});

// Resets the visualisation by just displaying a blank white space
function resetVisualisation() {
    $('#visualisation').innerHTML = "";
    $("#visualisation").html("");
}

// This is for rendering the geographical dot map
function geographicmap(showPrevalence, showDeathRate, showBoth) {
    displayNotification("Success", "Geographic(Dot) map visualisation chosen.");

    // defines the view of the geographic map
    var mapProp = {
        center: new google.maps.LatLng(30.3, 69.3),
        zoom: 5,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            }
        ]
    };

    var map = new google.maps.Map(document.getElementById("visualisation"), mapProp);

    var selectedRegIds = [];
    var selectedInfIds = [];
    var selectedBactIds = []; 

    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
            selectedRegIds.push(button.getAttribute("data-index"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
            if (button.getAttribute("data-type") === "I") {
                selectedInfIds.push(button.getAttribute("data-index"));
            } else {
                selectedBactIds.push(button.getAttribute("data-index"));
            }
        }
    });
    
    var result = window.ReceivedData;
    var prevGrandTotal = result[result.length - 2];
    var dtGrandTotal = result[result.length - 1];

    var infowindow = new google.maps.InfoWindow({
        maxWidth: 400
    });
    console.log("Results");
    console.log(result);
    for (var item in result) {
        var value = result[item];
        if (selectedRegIds.indexOf(value["RegionID"] + "") >= 0) {
            for (var subitem in value['Results']) {
                if ((value['Results'][subitem]["IsBacteria"] && selectedBactIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0) || (!value['Results'][subitem]["IsBacteria"] && selectedInfIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0)) { //if (selectedInfections.indexOf(value["Results"][subitem]["Name"]) >= 0) {

                    var infbact = value['Results'][subitem];

                    prevalences = infbact['TotalPrevalences'];
                    deathRates = infbact['TotalDeathRates'];

                    var infections = "";
                    for (var inf in infbact["BactInfections"]) {
                        var infection = infbact["BactInfections"][inf];
                        infections += '<a target="_blank" href=/Infections/Details/' + infection["Value"] + '>'+infection["Key"]+'</a>, ';
                    }
                    var bacterias = "";
                    for (var bact in infbact["InfBacteriaList"]) {
                        var bacteria = infbact["InfBacteriaList"][bact];
                        bacterias += '<a target="_blank" href=/Bacteria/Details/' + bacteria["Value"] + '>' + bacteria["Key"] + '</a>, ';
                    }
                    var attachments = "";
                    for (var att in infbact["Attachments"]) {
                        var attachment = infbact["Attachments"][att];
                        attachments += '<a target="_blank" href="/Utils/DownloadFile?link=' + attachment["Value"] + '&name=' + attachment["Key"] +'">' + attachment["Key"] + '</a>, ';
                    }
                    var clinicalInfos = "";
                    for (var clf in infbact["InfClinicalInformationList"]) {
                        var info = infbact["InfClinicalInformationList"][clf];
                        clinicalInfos += '<a target="_blank" href="/ClinicalInformations/Details/' + info["Value"] + '">' + info["Key"] + '</a>, ';
                    }

                    ModalDetails = "";
                    for (var occr in infbact["Occurrences"]) {
                        var oc = infbact["Occurrences"][occr];

                        if ((showBoth || (showPrevalence && showDeathRate)) && (oc["Prevalence"]<= 0 && oc["DeathRate"] <= 0)) {
                            continue;
                        } else if (showDeathRate && (!showPrevalence && !showBoth) && oc["DeathRate"] <= 0) {
                            continue;
                        } else if (showPrevalence && (!showDeathRate && !showBoth) && oc["Prevalence"] <= 0) {
                            continue;
                        }

                        var modalDetails = "";
                        var contentString = "";
                        if (infbact["IsBacteria"]) { // bacteria
                            modalDetails =
                            '<div id="content" style="color:black">' +
                            '<h5 id="firstHeading" class="firstHeading">Bacteria: <b><i>' + (infbact["Name"]=="null"?"-": infbact["Name"]) + '</i></b></h5>'+
                            '<h6> Introduction: <b>' + (infbact["BactIntroduction"] == "null" ? "-" :infbact["BactIntroduction"]) + '</b></h6 > ' +
                            '<div id="bodyContent">' +
                            '<p><b>Prevalence: <b>' + oc["Prevalence"]+ '</b> (Total in ' + (value['RegionName'] == "null" ? "-" : value["RegionName"]) +': ' + prevalences + ')</b><br/>' +
                            '<b>Death Rate: <b>' + oc["DeathRate"] + '</b> (Total in ' + (value['RegionName'] == "null" ? "-" : value["RegionName"]) + ': ' + deathRates + ')</b><br/>' +
                            '</p>' +
                            '<p><table style="width:100%">' +
                            '<tr><td><b>Adaptability</b></td><td>' + (infbact['Adaptability'] == "null" ? "-" : infbact['Adaptability']) + '</td>' +
                            '<tr><td><b>Aerobic/Anaerobic</b></td><td>' + (infbact['BactAerobicity'] == "null" ? "-" : infbact['BactAerobicity']) + '</td>' +
                            '<tr><td><b>Age Group</b></td><td>' + (infbact['AgeGroup'] == "null" ? "-" : infbact['AgeGroup']) + '</td>' +
                            '<tr><td><b>Antibiotics</b></td><td>' + (infbact['Antibiotic'] == "null" ? "-" : infbact['Antibiotic']) + '</td>' +
                            '<tr><td><b>Clinical Characteristics</b></td><td>' + (infbact['BactClinicalCharacteristics'] == "null" ? "-" : infbact['BactClinicalCharacteristics']) + '</td>' +
                            '<tr><td><b>District</b></td><td>' + (infbact['District'] == "null" ? "-" : infbact['District']) + '</td>' +
                            '<tr><td><b>Environmental</b></td><td>' + (infbact['Environmental'] == "null" ? "-" : infbact['Environmental']) + '</td>' +
                            '<tr><td><b>Genetics</b></td><td>' + (infbact['Genetics'] == "null" ? "-" : infbact['Genetics']) + '</td>' +
                            '<tr><td><b>Gram Staining</b></td><td>' + (infbact['BactGramStaining'] == "null" ? "-" : infbact['BactGramStaining']) + '</td>' +
                            '<tr><td><b>History</b></td><td>' + (infbact['InfHistory'] == "null" ? "-" : infbact['InfHistory']) + '</td>' +
                            '<tr><td><b>Infections</b></td><td>' + (infections == "null" || infections == ""? "-" : infections) + '</td>' +
                            '<tr><td><b>Interactions</b></td><td>' + (infbact['BactInteractions'] == "null" ? "-" : infbact['BactInteractions']) + '</td>' +
                            '<tr><td><b>Involving Diseases</b></td><td>' + (infbact['BactInvolvingDiseases'] == "null" ? "-" : infbact['BactInvolvingDiseases']) + '</td>' +
                            '<tr><td><b>Likeliness</b></td><td>' + (infbact['BactLikeness'] == "null" ? "-" : infbact['BactLikeness']) + '</td>' +
                            '<tr><td><b>Location</b></td><td>' + (oc['Location'] == "null" ? "-" : oc['Location']) + '</td>' +
                            '<tr><td><b>Mode of Transmissions</b></td><td>' + (infbact['ModeofTransmissions'] == "null" ? "-" : infbact['ModeofTransmissions']) + '</td>' +
                            '<tr><td><b>Morphology</b></td><td>' + (infbact['BactMorphology'] == "null" ? "-" : infbact['BactMorphology']) + '</td>' +
                            '<tr><td><b>NCBI Link</b></td><td> <a target="_blank" href="' + (infbact['BactNCBI'] == "null" ? "-" : infbact['BactNCBI']) + '">' + (infbact['BactNCBI'] == "null" ? "-" : infbact['BactNCBI']) + '</a></td>' +
                            '<tr><td><b>Occurrence</b></td><td>' + (infbact['BactOccurrence'] == "null" ? "-" : infbact['BactOccurrence']) + '</td>' +
                            '<tr><td><b>Oxygen Requirements</b></td><td>' + (infbact['BactOxygenRequirement'] == "null" ? "-" : infbact['BactOxygenRequirement']) + '</td>' +
                            '<tr><td><b>Shape</b></td><td>' + (infbact['BactShape'] == "null" ? "-" : infbact['BactShape']) + '</td>' +
                            '<tr><td><b>Strain</b></td><td>' + (infbact['BactStrain'] == "null" ? "-" : infbact['BactStrain']) + '</td>' +
                            '<tr><td><b>Taxonomy</b></td><td>' + (infbact['BactTaxonomy'] == "null" ? "-" : infbact['BactTaxonomy']) + '</td>' +
                            '<tr><td><b>Timeline</b></td><td>' + (oc['StartDate'] == "null" ? "-" : oc['StartDate']) + '&nbsp;To&nbsp;' + (oc['EndDate'] == "null" ? "-" : oc['EndDate'])+'</td>' +
                            '<tr><td><b>Treatments</b></td><td>' + (infbact['Treatment'] == "null" ? "-" : infbact['Treatment'])+ '</td>' +
                            '<tr><td><b>Virulence Factors</b></td><td>' + (infbact['BactVirulenceFactors'] == "null" ? "-" : infbact['BactVirulenceFactors'])+ '</td>' +
                            '<tr><td><b>References</b></td><td>' + (infbact['References'] == "null" ? "-" : infbact['References'])+ '</td>' +
                            '<tr><td><b>Attachments</b></td><td>' + attachments + '</td>';
                            '</div>' +
                                '</div>';

                            contentString = '<div id="content" style="color:black">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h5 id="firstHeading" class="firstHeading">' + infbact['Name'] + '</h5>' +
                                '<div id="bodyContent">' +
                                '<p><b>Prevalence: ' + oc["Prevalence"] + ' (Total in ' + value['RegionName']+': ' + prevalences + ')</b><br/>' +
                                '<b>Death Rate: ' + oc["DeathRate"] + ' (Total in ' + value['RegionName']+': ' + deathRates + ')</b><br/>' +
                                '<b>Type: Bacteria</b><br/>' +
                                '<p>Location: ' + oc['Location'] +'</p>' +
                                '<button type="button"  id="launchModal" class="btn btn-primary" onclick="launchModal()" data-toggle="modal" data-target="#exampleModalLong"> See All Details </button >' +
                                '</div>' +
                                '</div>';
                        } else { // infection
                            modalDetails =
                            '<div id="content" style="color:black">' +
                            '<h5 id="firstHeading" class="firstHeading">Infection: <b>' + infbact["Name"] + '</b></h5>' +
                            '<div id="bodyContent">' +
                            '<p><b>Prevalence: ' + oc["Prevalence"] + ' (Total in ' + value['RegionName'] + ': ' + prevalences + ')</b><br/>' +
                            '<b>Death Rate: ' + oc["DeathRate"] + ' (Total in ' + value['RegionName'] + ': ' + deathRates + ')</b><br/>' +
                            '<p><table style="width:100%">' +
                            '<tr><td><b>Adaptability</b></td><td>' + (infbact['Adaptability'] == "null" ? "-" : infbact['Adaptability']) + '</td>' +
                            '<tr><td><b>Age Group</b></td><td>' + (infbact['AgeGroup'] == "null" ? "-" : infbact['AgeGroup']) + '</td>' +
                            '<tr><td><b>Antibiotics</b></td><td>' + infbact['Antibiotic'] + '</td>' +
                            '<tr><td><b>Bacteria</b></td><td>' + bacterias + '</td></tr>' +
                            '<tr><td><b>Classification</b></td><td>' + infbact['InfClassification'] + '</td>' +
                            '<tr><td><b>Clinical Information</b></td><td>' + clinicalInfos + '</td>' +
                            '<tr><td><b>Definition</b></td><td>' + infbact['Definition'] + '</td>' +
                            '<tr><td><b>Diagnosis</b></td><td>' + infbact['InfDiagnosis'] + '</td>' +
                            '<tr><td><b>District</b></td><td>' + (infbact['District'] == "null" ? "-" : infbact['District']) + '</td>' +
                            '<tr><td><b>Environmental</b></td><td>' + (infbact['Environmental'] == "null" ? "-" : infbact['Environmental']) + '</td>' +
                            '<tr><td><b>Epidemiology</b></td><td>' + infbact['InfEpidemiology'] + '</td>' +
                            '<tr><td><b>Genetics</b></td><td>' + (infbact['Genetics'] == "null" ? "-" : infbact['Genetics']) + '</td>' +
                            '<tr><td><b>History</b></td><td>' + infbact['InfHistory'] + '</td>' +
                            '<tr><td><b>Location</b></td><td>' + oc['Location'] + '</td>' +
                            '<tr><td><b>Mode of Transmissions</b></td><td>' + infbact['ModeofTransmissions'] + '</td>' +
                            '<tr><td><b>Pathophysiology</b></td><td>' + infbact['InfPathophysiology'] + '</td>' +
                            '<tr><td><b>Prevention</b></td><td>' + infbact['InfPrevention'] + '</td>' +
                            '<tr><td><b>Seasonal</b></td><td>' + infbact['InfSeasonal'] + '</td>' +
                            '<tr><td><b>Symptoms</b></td><td>' + infbact['InfSymptoms'] + '</td>' +
                            '<tr><td><b>Timeline</b></td><td>' + oc['StartDate'] + '&nbsp;To&nbsp;' + oc['EndDate'] + '</td>' +
                            '<tr><td><b>Treatments</b></td><td>' + infbact['Treatment'] + '</td>' +
                            '<tr><td><b>Type</b></td><td>' + infbact['InfType'] + '</td>' +
                            '<tr><td><b>References</b></td><td>' + infbact['References'] + '</td>' +
                            '<tr><td><b>Attachments</b></td><td>' + attachments + '</td>' +
                            '</div>' +
                            '</div>';

                            contentString = '<div id="content" style="color:black">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h5 id="firstHeading" class="firstHeading">' + infbact['Name'] + '</h5>' +
                                '<div id="bodyContent">' +
                                '<p><b>Prevalence: ' + oc["Prevalence"] + ' (Total in ' + value['RegionName'] + ': ' + prevalences + ')</b><br/>' +
                                '<b>Death Rate: ' + oc["DeathRate"] + ' (Total in ' + value['RegionName'] + ': ' + deathRates + ')</b><br/>' +
                                '<b>Type: Infection</b><br/>' +
                                '<p>Location: ' + oc['Location'] + '</p>' +
                                '<button type="button"  id="launchModal" class="btn btn-primary" onclick="launchModal()" data-toggle="modal" data-target="#exampleModalLong"> See All Details </button >' +
                                '</div>' +
                                '</div>';
                        }
                        var lat = parseFloat(oc['Latitude']);
                        var long = parseFloat(oc['Longitude']);
                        var opacity = 0.5;//oc["Prevalence"] / prevGrandTotal;
                        if (showBoth || (showPrevalence && showDeathRate)) {
                            opacity = (oc["Prevalence"] + oc["DeathRate"]) / (prevGrandTotal + dtGrandTotal);
                            displayNotification("Success", "Colour of dots depends on the Prevalences and Death Rates.");
                        } else if (showDeathRate) {
                            opacity = (oc["DeathRate"]) / (dtGrandTotal);
                            displayNotification("Success", "Colour of dots depends on the Death Rates.");
                        } else if (showPrevalence) {
                            opacity = (oc["Prevalence"]) / (prevGrandTotal);
                            displayNotification("Success", "Colour of dots depends on the Prevalences.");
                        } else {
                            opacity = 0.2;
                        }
                        opacity = opacity < 0.2 ? 0.2 : opacity;

                        var marker = new google.maps.Marker({
                            position: { lat: lat, lng: long },
                            map: map,
                            icon: getCircle(4, opacity)
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, content, details) {
                            return function () {
                                infowindow.setContent(content);
                                infowindow.open(map, marker);
                                window.ModalDetails = details;
                            }
                        })(marker, contentString, modalDetails));
                        

                    }
                }
            }
        }
    }    
}

function launchModal() {
    document.getElementById('modal-body').innerHTML = window.ModalDetails;
}

// return the circle object to be placed on the geographical dot map
function getCircle(magnitude, opacity) {
    return {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: 'red',
        fillOpacity: opacity,
        scale: Math.pow(2, magnitude) / 2,
        strokeColor: 'black',
        strokeWeight: .6
    };
}

// renders the choropleth map using the cached visualisation data
function choropleth(showPrevalence, showDeathRate, showBoth) {
    displayNotification("Success", "Choropleth map visualisation chosen.");

    var mapProp = {
        center: new google.maps.LatLng(30.3, 69.3),
        zoom: 5,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            }
        ]
    };

    var map = new google.maps.Map(document.getElementById("visualisation"), mapProp);

    var selectedRegions = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
        }
    });

    var selectedInfections = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });
    
    var results = [];
    var result = window.ReceivedData;
    
    var maxDt = 0; var maxPrev = 0;
    
    for (var item = 0; item < result.length-3; item++) {
        var value = result[item];
        var push = {};
        if (value['RegionName'] == null || selectedRegions.indexOf(value['RegionName']) < 0) {
            continue;
        }
        push.region = value['RegionName'];

        var totalprevalences = 0;
        var totaldeaths = 0;
        for (var subitem in value['Results']) {
            if (selectedInfections.indexOf(value["Results"][subitem]["Name"]) >= 0) {
                totalprevalences += value['Results'][subitem]['TotalPrevalences'];
                totaldeaths += value['Results'][subitem]['TotalDeathRates'];
            }
        }

        push.prev = totalprevalences;
        push.dt = totaldeaths;
        results.push(push);
        maxPrev = totalprevalences > maxPrev ? totalprevalences : maxPrev;
        maxDt = totaldeaths > maxDt ? totaldeaths : maxDt; 
    }
    
    var infowindow = new google.maps.InfoWindow();
    map.data.addListener('click', function (event) {
        var str = "<div style='width:auto; color: #0f0f0f'>" +
            "<table>" +
            "<tr> <td>Location: </td><td>" + event.feature.getProperty('districts') + ", " + event.feature.getProperty('province_territory') + "</td>" +
            "<tr><td>Prevalence: </td><td>0</td></tr>" +
            "<tr><td>Death Rate: </td><td>0</td></tr>" +
            "</table></div>";
        for (var i in results) {
            var item = results[i];
            if (item.region.toLowerCase() == event.feature.getProperty('districts').toLowerCase()) {
                str = "<div style='width:auto; color: #0f0f0f'>" +
                    "<table>" +
                    "<tr> <td>Location: </td><td>" + event.feature.getProperty('districts') + ", " + event.feature.getProperty('province_territory') + "</td>" +
                    "<tr><td>Prevalence: </td><td>"+item.prev+"</td></tr>" +
                    "<tr><td>Death Rate: </td><td>" + item.dt +"</td></tr>" +
                    "</table></div>";
                break;
            }
        }
        infowindow.setContent(str);
        map.data.revertStyle();
        map.data.overrideStyle(event.feature, { strokeWeight: 2 });

        infowindow.setPosition(event.latLng);
        infowindow.setOptions({ pixelOffset: new google.maps.Size(0,0) });
        infowindow.open(map);
    });


    if (showDeathRate || (showDeathRate && !showPrevalence && !showPrevalence)) {
        setScale(maxDt);
    } else {
        setScale(maxPrev);
    }

    var countries = result[result.length - 3];
    for (var c in countries) {
        var country = countries[c];
        map.data.loadGeoJson('/Assets/map/' + country);//'@Url.Content("~/Assets/map/pakistan.json")');
    }
    map.data.setStyle(function (feature) {
        var color = "#ffffff";
        for (var i in results) {
            var item = results[i];
            if (item.region == feature.getProperty('districts')) {
                if (showDeathRate || (showDeathRate && !showPrevalence && !showPrevalence)) {
                    color = getChoroplethColour(item.dt, maxDt)
                    displayNotification("Success", "Colours are dependent on the Death Rates.");
                } else {
                    color = getChoroplethColour(item.prev, maxPrev);
                    displayNotification("Success", "Colours are dependent on the Prevalences.");
                }
                break;
            }
        }
        return {
            fillColor: color,
            strokeWeight: 0.5,
            fillOpacity:1.0
        };
    });

}

// return the colour for the choropleth region
function getChoroplethColour(value, max) {
    var Colours = ['#fff7ec','#fee8c8','#fdd49e','#fdbb84','#fc8d59','#ef6548','#d7301f','#b30000','#7f0000'];

    for (var i = 0; i < 9; i++) {
        var val = Math.ceil((i * max) / (Colours.length));
        if (i == 8 && value > val && value <= max) {
            return Colours[i];
        } else if (value <= val) {
            return Colours[i];
        }
    }
}

// renders the scale for the choropleth map
function setScale(max) {
    var newScale = [];
    if (max>0) {
        var colours = 9;
        var scale = [];
        for (var i = 0; i < 9; i++) {
            scale.push(Math.ceil((i * max) / (colours)));
        }
        for (var i = 0; i < scale.length; i++) {
            if (i + 1 >= colours) {
                newScale.push("<=" + max);
                break;
            }
            if (scale[i] == scale[i + 1]) {
                newScale.push("<i>N/A</i>");
            } else {
                newScale.push("<=" + scale[i]);
            }
        }
    } else {
        newScale.push("<=10");
        newScale.push("<=20");
        newScale.push("<=30");
        newScale.push("<=40");
        newScale.push("<=50");
        newScale.push("<=60");
        newScale.push("<=70");
        newScale.push("<=80");
        newScale.push("<=90");
    }
    $('#scaleOne').html(newScale[0]);
    $('#scaleTwo').html(newScale[1]);
    $('#scaleThree').html(newScale[2]);
    $('#scaleFour').html(newScale[3]);
    $('#scaleFive').html(newScale[4]);
    $('#scaleSix').html(newScale[5]);
    $('#scaleSeven').html(newScale[6]);
    $('#scaleEight').html(newScale[7]);
    $('#scaleNine').html(newScale[8]);
}

// this is a utility methods to make date in the correct format
function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

// renders the line graph
function lineGraph(showPrevalence, showDeathRate, showBoth) {
    displayNotification("Success", "Line graph visualisation chosen.");

    resetVisualisation();

    var selectedRegions = [];
    var selectedRegIds = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            selectedRegions.push(button.getAttribute("data-id"));
            selectedRegIds.push(button.getAttribute("data-index"));
        }
    });

    var selectedInfections = [];
    var selectedInfIds = [];
    var selectedBactIds = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            if (button.getAttribute("data-type") === "I") {
                selectedInfIds.push(button.getAttribute("data-index"));
            } else {
                selectedBactIds.push(button.getAttribute("data-index"));
            }
            selectedInfections.push(button.getAttribute("data-id"));
        }
    });

    var startDate = new Date($('#fromDateChoice')[0].value);
    var endDate = new Date($('#toDateChoice')[0].value);
    var dates = [];
    var iDate = new Date(startDate);
    while (iDate < endDate) {
        iDate.setMonth(iDate.getMonth() + 1);
        dates.push(iDate.getFullYear() + "-" + pad("0" + iDate.getMonth(), 2));
    }

    var infectionsPrevalence = {};
    var infectionsDeathrate = {};
    var result = window.ReceivedData
    for (var item in result) {
        var value = result[item];
        if (selectedRegIds.indexOf(value["RegionID"] + "") >= 0) {
            for (var subitem in value['Results']) {
                if ((value['Results'][subitem]["IsBacteria"] && selectedBactIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0) || (!value['Results'][subitem]["IsBacteria"] && selectedInfIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0)) {//if (selectedInfections.indexOf(value["Results"][subitem]["Name"]) >= 0) {
                    prevalences = value['Results'][subitem]['TotalPrevalences'];
                    deathRates = value['Results'][subitem]['TotalDeathRates'];

                    occurrences = value["Results"][subitem]["OccurrencesOriginal"];
                    for (var occurrence in occurrences) {
                        if (infectionsPrevalence[occurrences[occurrence]['EndDate']] == null) {
                            infectionsPrevalence[occurrences[occurrence]['EndDate']] = occurrences[occurrence]["Prevalence"];
                            infectionsDeathrate[occurrences[occurrence]['EndDate']] = occurrences[occurrence]["DeathRate"];
                        } else {
                            infectionsPrevalence[occurrences[occurrence]['EndDate']] += occurrences[occurrence]["Prevalence"];
                            infectionsDeathrate[occurrences[occurrence]['EndDate']] += occurrences[occurrence]["DeathRate"];
                        }
                    }
                }
            }
        }
    }
    var data = [];
    var labels = []; ykeys = [];
    var usedDates = [];
    if ((showDeathRate && showPrevalence) || showBoth) {
        for (var i in infectionsPrevalence) {
            var push = {};
            push.x = i;
            push.y = infectionsPrevalence[i];
            push.z = infectionsDeathrate[i];
            usedDates.push(i);
            data.push(push);
        }
        for (var d in dates) {
            if (usedDates.indexOf(dates[d]) < 0) {
                data.push({x:dates[d], y:0, z:0});
            }
        }
        labels = ["Prevalence", "Death Rate"];
        ykeys = ['y', 'z'];
        displayNotification("Success", "Displaying separate lines for prevalences and death rates.");
    } else if (showDeathRate) {
        for (var i in infectionsDeathrate) {
            var push = {};
            push.x = i;
            push.y = infectionsDeathrate[i];
            usedDates.push(i);
            data.push(push);
        }
        for (var d in dates) {
            if (usedDates.indexOf(dates[d]) < 0) {
                data.push({ x: dates[d], y: 0});
            }
        }
        labels = ["Death Rate"];
        ykeys = ['y'];
        displayNotification("Success", "Displaying the death rates in the line graph.");
    } else {// show prevalence
        for (var i in infectionsPrevalence) {
            var push = {};
            push.x = i;
            push.y = infectionsPrevalence[i];
            usedDates.push(i);
            data.push(push);
        }
        for (var d in dates) {
            if (usedDates.indexOf(dates[d]) < 0) {
                data.push({ x: dates[d], y: 0});
            }
        }
        ykeys = ['y'];
        labels = ["Prevalence"];
        displayNotification("Success", "Displaying the prevalences in the line graph.");
    }
    
    $('#visualisation').css("padding", "50px");

    Morris.Line({
        element: 'visualisation',
        data: data,
        xkey: 'x',
        ykeys: ykeys,
        smooth: false,
        hideHover: 'auto',
        axes: true,
        grid: true,
        labels: labels,
        lineColors: ["#4250f4", '#f44141']
    });
}

// renders the scatter plot
function scatter(showPrevalence, showDeathRate, both) {
    displayNotification("Success", "Scatter plot visualisation chosen.");

    //var selectedRegions = [];
    var selectedRegIds = [];
    var regs = document.getElementById('regions_div').getElementsByTagName('button');
    $.each(regs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            //selectedRegions.push(button.getAttribute("data-id"));
            selectedRegIds.push(button.getAttribute("data-index"));
        }
    });

    //var selectedInfections = [];
    var selectedInfIds = [];
    var selectedBactIds = [];
    var infs = document.getElementById('infections_div').getElementsByTagName('button');
    $.each(infs, function (index, button) {
        if (button.classList.contains("vis-badge-selected")) {
            if (button.getAttribute("data-type") === "I") {
                selectedInfIds.push(button.getAttribute("data-index"));
            } else {
                selectedBactIds.push(button.getAttribute("data-index"));
            }
            //selectedInfections.push(button.getAttribute("data-id"));
        }
    });

    var regions = [];
    var infs = [];
    var results = [];
    var result = window.ReceivedData;
    for (var item in result) {
        var value = result[item];
        if (selectedRegIds.indexOf(value["RegionID"] + "") >= 0) {
            if (regions.indexOf(value["RegionName"]) < 0) { regions.push(value['RegionName']); }
            var deathrates = {};
            var prevs = {};
            for (var subitem in value['Results']) {
                var totalprevalences = 0;
                var totaldeaths = 0;
                if ((value['Results'][subitem]["IsBacteria"] && selectedBactIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0) || (!value['Results'][subitem]["IsBacteria"] && selectedInfIds.indexOf((value['Results'][subitem]["ID"]) + "") >= 0)) {//if (selectedInfections.indexOf(value["Results"][subitem]["Name"]) >= 0) {
                    prevs[value["Results"][subitem]["Name"]] = value['Results'][subitem]['TotalPrevalences'];
                    deathrates[value["Results"][subitem]["Name"]] = value['Results'][subitem]['TotalDeathRates'];
                    if (infs.indexOf(value["Results"][subitem]["Name"]) < 0) {
                        infs.push(value["Results"][subitem]["Name"]);
                    }
                }
            }
            results[value['RegionName']] = [prevs, deathrates];
        }
    }

    var traces = [];
    var maxYVal = 0;
    var minYVal = 0;
    var minInitialised = false;
    
    if ((showDeathRate && !showPrevalence) || showDeathRate) {
        for (var inf in infs) {
            var yVals = [];
            for (var reg in regions) {
                var val = results[regions[reg]][1][infs[inf]];
                val = val ? val : 0
                if (!minInitialised) {
                    minYVal = val;
                    minInitialised = true;
                }
                maxYVal = val > maxYVal ? val : maxYVal;
                minYVal = val < minYVal ? val : minYVal;
                yVals.push(val);
            }
            var trace = {};
            trace.x = regions;
            trace.y = yVals;
            trace.mode = "markers";
            trace.type = "scatter";
            trace.name = infs[inf];
            trace.marker = { size: 12 };
            traces.push(trace);
        }
        minYVal = minYVal - 5 < 0 ? 0 : minYVal - 5;

        var layout = {
            xaxis: {
                range: [-1, regions.length + 1], title: "Regions", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            yaxis: {
                range: [minYVal, maxYVal+5], title: "Death Rates", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            title: 'Death Rates of Selected Infections/Bacteria at Selected Regions'
        };
        displayNotification("Success", "Scatter plot is based on Death Rates.");
    } else {
        
        for (var inf in infs) {
            var yVals = [];
            for (var reg in regions) {
                var val = results[regions[reg]][0][infs[inf]];
                val = val ? val : 0
                if (!minInitialised) {
                    minYVal = val;
                    minInitialised = true;
                }
                maxYVal = val > maxYVal ? val : maxYVal;
                minYVal = val < minYVal ? val : minYVal;
                yVals.push(val);
            }
            var trace = {};
            trace.x = regions;
            trace.y = yVals;
            trace.mode = "markers";
            trace.type = "scatter";
            trace.name = infs[inf];
            trace.marker = { size: 12 };
            traces.push(trace);
        }
        minYVal = minYVal - 5 < 0 ? 0 : minYVal - 5;
        var layout = {
            xaxis: {
                range: [-1, regions.length + 1], title: "Regions", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            yaxis: {
                range: [minYVal, maxYVal+5], title: "Prevalence", titlefont: {
                    family: 'Arial, sans-serif',
                    size: 18,
                    color: 'lightgrey'
                }
            },
            title: 'Prevalence of Selected Infections/Bacteria at Selected Regions'
        };
        displayNotification("Success", "Scatter plot is based on Prevalences.");
    }
    Plotly.newPlot('visualisation', traces, layout);
    $('#visualisation').css("height", "100%");
    $('#visualisation').css("width", "100%");
    $('#visualisation').css("bottom-right", "40px"); 

}