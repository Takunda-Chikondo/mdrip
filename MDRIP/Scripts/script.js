﻿function Delete(url) {
    if (confirm('Are you sure you want to delete this record ?') === true) {
        $.ajax({
            type: 'Post',
            url: url,
            success(response) {
                $("#data").html(response.html);
                $.notify(response.message, "success");
            }
        });
    }
}