Team Details:
1. Kushal S S Cuppoor, CPPKUS001
2. Takunda Chikondo, CHKTAK002

Link to hosted webapp: mdrip.azurewebsites.net

Work Breakdown:
1. Kushal - CPPKUS001
	
	Visualising multidrug resistant infections/bacteria data
	Specific files for visualisation components include:
	a. ~/MDRIP/Views/Visualisations/*
	b. ~/MDRIP/Scripts/script/visualisation.js
	c. ~/MDRIP/Controllers/VisualisationsController.cs
	d. ~/MDRIP/Models/VisualisationData.cs
	e. ~/MDRIP/Models/RegionVisDataBundle.cs
	f. ~/MDRIP/Content/Style/mycss.css (this file is common for both team member)

	
		
2. Takunda

	Reporting multidrug resistant infections/bacteria data
	Specific files: Most of the remaining ones. 